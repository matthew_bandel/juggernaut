# sandboxers.py to develop TLCF interface

# import reload
from importlib import reload

# import general tools
import os
import sys
import json
import re

# import yaml for reading configuration file
import yaml

# import time and datetime
import time
import datetime

# import h5py to read h5 files
import h5py

# import math functions
import math
import numpy

# import local classes
from features import Feature
from formulas import Formula
from hydras import Hydra


# class Juggernaut to do OMI data reduction
class Sandboxer(Hydra):
    """Sandboxer class to develop TLCF / ACPS interface structure.

    Inherits from:
        Hydra
    """

    def __init__(self, sink, source, start='', finish=''):
        """Initialize a Juggernaut instance.

        Arguments:
            sink: str, filepath for data dump
            source: str, filepath of source files
            start: str, date-based subdirectory
            finish: str, date-based subdirectory

        Returns:
            None
        """

        # initialize the base Hydra instance
        super().__init__(sink, source, start, finish)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Sandboxer instance at: {} >'.format(self.sink)

        return representation

    def bound(self):
        """Find the latitude and longitude bounding boxes for the files.

        Arguments:
            None

        Returns:
            None
        """

        # make a bounds folder
        self._make('{}/bounds'.format(self.sink))

        # open up the control file
        control = self._acquire('{}/controls/control.yaml'.format(self.sink))
        paths, destinations = [control[field] for field in ('Inputs', 'Outputs')]

        # for each path
        for path in paths:

            # ingest
            self.ingest(path)

            # get banded features
            features = self.apply(lambda feature: 'BAND' in feature.slash)

            # sort by band
            bands = self._group(features, lambda feature: re.search('BAND[1-3]', feature.slash).group())

            # go through bands
            for band, members in bands.items():

                # print survey
                self.survey()

                # begin formula
                formula = Formula()

                # grab latitude bounds and longitude bounds
                formula.formulate('latitude_bounds')
                formula.formulate('longitude_bounds')

                # calculate reductions
                self._stamp('cascading categories...')
                categories = self.cascade(formula, members)

                # insert into Categories and link to Data
                [feature.divert('Categories') for feature in categories]
                [feature.update({'link': True}) for feature in categories]

                # grab the independent variables from file path
                self._stamp('creating independents...')
                independents = self._parse(path)
                independents = {field: numpy.array([quantity]) for field, quantity in independents.items()}
                independents = [Feature(['IndependentVariables', name], array) for name, array in independents.items()]

                # create destination name and stash featues
                self._stamp('stashing...')
                stage = self._stage(path)
                orbit = stage['orbit']
                date = stage['date']
                product = stage['product']
                tags = (self.sink, product, band, date, orbit)
                destination = [name for name in destinations if all([tag in name for tag in tags])][0]
                self._stash(independents + categories, destination, 'Data')

        return None

    def control(self):
        """Generate a control file.

        Arguments:
            None

        Returns:
            None
        """

        # create control directory
        self._make('{}/controls'.format(self.sink))

        # begin specifics
        now = self._note()
        specifics = {'Inputs': [], 'Outputs': []}

        # create products-bands mapping
        products = {'L1-OML1BRUG': ['BAND1', 'BAND2'], 'L1-OML1BRVG': ['BAND3']}

        # for each path
        for path in self.paths:

            # add to inputs
            specifics['Inputs'].append(path)

            # stage the path
            stage = self._stage(path)

            # unpack stage
            orbit, date, product = [stage[field] for field in ('orbit', 'date', 'product')]

            # get bands
            bands = products[product]

            # for each band
            for band in bands:

                # create destination and add
                formats = (self.sink, product, band, date, orbit, now)
                destination = '{}/bounds/OMI_Bounds_{}_{}_{}-o{}_{}.h5'.format(*formats)
                specifics['Outputs'].append(destination)

        # dump into control
        self._dispense(specifics, '{}/controls/control.yaml'.format(self.sink))

        return None