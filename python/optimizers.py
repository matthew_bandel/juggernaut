# import reload
from importlib import reload

# import tools
from cores import Core


# class Optimizer
class Optimizer(Core):
    """class Optimizier to compare juggernaut versions performances.

    Inherits from:
        Core
    """

    def __init__(self):
        """Initiate instance.

        Argumenets:
            None
        """

        return

    def _match(self, joldo, juggo):
        """Match primary keys between joldo and juggo.

        Arguments:
            joldo: dict
            juggo: dict

        Returns:
            list of (str, str) tuples
        """

        # begin matches
        matches = []
        lines = []

        # go through each joldo key
        for key in joldo.keys():

            # begin extents
            extents = []

            # get all juggo keys
            for keyii in juggo.keys():

                # split on /
                slashes = keyii.replace('_fraction', '').replace('_count', '').split('/')

                # check for all
                if all([slash in key for slash in slashes]):

                    # add extents
                    extents.append(keyii)

            # if found
            if len(extents) > 0:

                # sort extents by total length
                extents.sort(key=lambda extent: len(extent), reverse=True)

                # add to matches
                matches.append((key, extents[0]))
                lines.append(key)
                lines.append(extents[0])
                lines.append(' ')

        # transcibe
        self._jot(lines, 'studies/sandboxer/timing/matches.txt')

        return matches

    def _rationalize(self, matches, joldo, juggo):
        """Compute ratios between all calculations.

        Arguments:
            matches: list of (str, str) tuples
            joldo: dict
            juggo: dict

        Return:
            None
        """

        # begin ratios
        ratios = []

        # for each key pair
        for key, keyii in matches:

            # for each operation
            for operation in joldo[key].keys():

                # get time
                time = joldo[key][operation]

                # for each secondary key
                timeii = 0
                for operationii in juggo[keyii].keys():

                    # check for operation
                    if operation in operationii:

                        # get secondary time
                        timeii = juggo[keyii][operationii]

                # compute ratios
                ratio = timeii / time

                # connstruct element
                elements = (ratio, timeii, time, operation, keyii, key)
                ratios.append(elements)

        # sort ratios
        ratios.sort(key=lambda elements: elements[0], reverse=True)
        #ratios.sort(key=lambda elements: elements[1], reverse=True)

        # create lines
        lines = [' '.join([str(element) for element in elements]) for elements in ratios]
        self._jot(lines, 'studies/sandboxer/timing/ratios.txt')

        print('total juggo: {}'.format(sum([elements[1] for elements in ratios])))
        print('total joldo: {}'.format(sum([elements[2] for elements in ratios])))

        return None

    def compare(self):

        # make core
        core = Core()

        # transcribe output files
        juggo = core._load('studies/sandboxer/timing/juggo.json')
        joldo = core._load('studies/sandboxer/timing/joldo.json')

        # match all keys
        matches = self._match(joldo, juggo)

        # compute all ratios
        ratios = self._rationalize(matches, joldo, juggo)

        return None