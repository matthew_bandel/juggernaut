#!/usr/bin/env python3

# heimdals.py for the Heimdal class to crossreference omi and omps orbits

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import regex
import re

# import numpy functions
import numpy

# import datetime
import datetime

# import sklearn
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.neighbors import KDTree


# class Millipede to do OMI data reduction
class Heimdal(Core):
    """Heimdal class to concatenate single orbit files.

    Inherits from:
        Core
    """

    def __init__(self):
        """Initialize instance.

        Arguments:
            None
        """

        # initialize the base Core instance
        Core.__init__(self)

        # keep hydras
        self.hydra = None
        self.hydraii = None

        # keep records
        self.records = []

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Heimdal instance >'

        return representation

    def _crystalize(self, data, latitudes, longitudes, segment=20):
        """Bin the geographic data by percentile bins.

        Arguments:
            data: numpy array of measurement data
            latitudes: numpy array of associated latitudes
            longitudes: numpy array of assoicated longitudes
            segment=20: size of bin

        Returns:
            list of numpy arrays, data parsed by bin
        """

        # define percentile markers
        percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
        percentiles = [numpy.percentile(data, percent) for percent in percents]

        # begin geodes
        geodes = []

        # for each bracket
        brackets = [(first, second) for first, second in zip(percentiles[:-1], percentiles[1:])]
        for first, second in brackets:

            self._print(first, second)

            # create mask for particular range
            mask = (data >= first) & (data <= second)

            # get all data in the bin
            datum = data[mask].flatten()
            latitude = latitudes[mask].flatten()
            longitude = longitudes[mask].flatten()

            print(' ')
            print('datum: {}'.format(datum.shape))
            print('latitude: {}, {} - {}'.format(latitude.shape, latitude.min(), latitude.max()))
            print('longitude: {}, {} - {}'.format(longitude.shape, longitude.min(), longitude.max()))

            # concatenate
            geode = numpy.vstack([datum, latitude, longitude])

            print('geode: {}'.format(geode.shape))

            # order along latitude, then longitude
            geode = geode[:, numpy.argsort(latitude)]
            geode = geode[:, numpy.argsort(longitude)]

            print('geode: {}'.format(geode.shape))

            # append
            geodes.append(geode)

        print('crstualizie:')
        print('geodes:', len(geodes))
        print('brackets:', len(brackets))

        return geodes, brackets

    def imagine(self, sink):
        """Create the h5 file for viewing in boquette.

        Arguments:
            sink: folder for deposit

        Returns:
            None
        """

        # make sink folder
        self._make(sink)

        # grab the top record
        record = self.records[0]

        # desginate hydras and sensors
        sensors = ('omi', 'omps')
        hydras = (self.hydra, self.hydraii)
        breadths = {'omi': 10, 'omps': 5}

        # begin preessures, etc
        pressures = []
        latitudes = []
        longitudes = []

        # for each sensor
        for sensor, hydra in zip(sensors, hydras):

            # digest the path for omi
            paths = [path for path in hydra.paths if record[sensor][3] in path]
            hydra.ingest(paths[0])

            # get the latitude, longitude, and pressuree data, and flatten
            step = record[sensor][5]
            breadth = breadths[sensor]
            pressures += hydra.dig('CloudPressureforO3')[0].distil()[step - breadth: step + breadth].flatten()
            latitudes += hydra.dig('Latitude')[0].distil()[step - breadth: step + breadth].flatten()
            longitudes += hydra.dig('Longitude')[0].distil()[step - breadth: step + breadth].flatten()

        # bin the data according to quantiles
        geodes, brackets = self._crystalize(pressures, latitudes, longitudes)

        print('imaggine:')
        print('geodes:', len(geodes))
        print('brackets:', len(brackets))

        # for each geode
        features = []
        for index, (geode, bracket) in enumerate(zip(geodes, brackets)):

            # create feature for latitude
            formats = [str(index).zfill(2)]
            formats += [str(round(bracket[0], 1)).zfill(5)]
            formats += [str(round(bracket[1], 1)).zfill(5)]
            name = 'cloud_pressure_latitude_{}_{}-{}'.format(*formats)

            # create feature and append
            feature = Feature(['Categories', name], geode[1])
            features.append(feature)

            # create abscissas for longitude
            formats = [str(index).zfill(2)]
            formats += [str(round(bracket[0], 1)).zfill(5)]
            formats += [str(round(bracket[1], 1)).zfill(5)]
            name = 'cloud_pressure_longitude_{}_{}-{}'.format(*formats)

            # create feature and append
            feature = Feature(['IndependentVariables', name], geode[2])
            features.append(feature)

        # stash file
        destination = '{}/Cloud_Pressures_OMI_OMPS_{}.h5'.format(sink, self._note())
        hydras[0]._stash(features, destination, 'Data')

        return None

    def peer(self, year, month, day):
        """Find the closest latitude and longitude match for a particular day.

        Arguments:
            year: int, year
            month: int, month
            day: int, day

        Returns:
            None
        """

        # make omi hydra instance
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}/{}'.format(year, str(month).zfill(2), str(day).zfill(2)))
        self.hydra = hydra

        # for each path
        locations = []
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # grab all geo information
            times = hydra.dig('Time')[0].distil() / 3600
            latitudes = hydra.dig('Latitude')[0].distil()
            longitudes = hydra.dig('Longitude')[0].distil()
            dates = [hydra._stage(path)['date']] * len(times)
            steps = list(range(len(times)))

            # got through each row
            for row in range(60):

                # zip together
                rows = [row] * len(times)
                zipper = list(zip(times, latitudes[:, row], longitudes[:, row], dates, rows, steps))

                # only keep those between +/- 40 latitude
                zipper = [quartet for quartet in zipper if -40 < float(quartet[1]) < 40]

                # add all locations
                locations += zipper

        # make npp hydra instance
        hydraii = Hydra('/tis/OMPS-NPP/60000/NMCLDRR-L2/{}/{}/{}'.format(year, str(month).zfill(2), str(day).zfill(2)))
        self.hydraii = hydraii

        # for each path
        locationsii = []
        for path in hydraii.paths:

            # ingest
            hydraii.ingest(path)

            # grab all geo information
            times = hydraii.dig('Time')[0].distil() / 3600
            latitudes = hydraii.dig('Latitude')[0].distil()
            longitudes = hydraii.dig('Longitude')[0].distil()
            dates = [hydraii._stage(path)['date']] * len(times)
            steps = list(range(len(times)))

            # got through each row
            for row in range(36):

                # zip together
                rows = [row] * len(times)
                zipper = list(zip(times, latitudes[:, row], longitudes[:, row], dates, rows, steps))

                # only keep those between +/- 40 latitude
                zipper = [quartet for quartet in zipper if -40 < float(quartet[1]) < 40]

                # add all locations
                locationsii += zipper

        # create tree from omi locations
        self._stamp('making tree...', initial=True)
        matrix = numpy.array([quartet[:3] for quartet in locations])
        tree = KDTree(matrix)

        # get the distance to second location
        self._stamp('asking tree...')
        matrixii = numpy.array([quartet[:3] for quartet in locationsii])
        distances, indices = tree.query(matrixii, k=1)

        # create records
        self._stamp('organizing locations...')
        records = []
        indexii = 0
        for distance, index in zip(distances, indices):

            # create record
            record = {'distance': distance[0], 'omi': locations[index[0]], 'omps': locationsii[indexii]}
            record.update({'hours': locations[index[0]][0] - locationsii[indexii][0]})
            record.update({'latitudes': locations[index[0]][1] - locationsii[indexii][1]})
            record.update({'longitudes': locations[index[0]][2] - locationsii[indexii][2]})

            # add record
            records.append(record)

            # increment
            indexii += 1

        # sort by distance
        records.sort(key=lambda record: record['distance'])
        self.records = records
        for record in records[:5]:

            # print
            self._look(record, 2)

        return None