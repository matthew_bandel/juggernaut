# import system tools
import datetime
import sys
import subprocess

# set up main function to run from command line
if __name__ == '__main__':

    # get times from arguments
    sink, year, month, start, finish = sys.argv[-5:]

    # construct TLCF source folder
    source = '/tis/OMI/10004/OML1BRUG/{}/{}'.format(year, month)

    # construct subprocess call
    call = ['python', 'juggernauts.py', sink, source, start, finish, '--control']
    print(call)

    # make call
    subprocess.call(call)
