# set up main function to run from command line
if __name__ == '__main__':

    # import juggernauts
    import juggernauts
    juggernaut = juggernauts.Juggernaut('studies/sandboxer', 'studies/sandboxer/data')
    juggernaut.control()
    juggernaut.rampage()
