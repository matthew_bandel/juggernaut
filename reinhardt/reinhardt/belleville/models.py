# import django models
from django.db import models
from django.contrib.auth.models import User


# import datetime
from datetime import datetime


# define sensor model
class Sensor(models.Model):

    # define sensor attributes
    name = models.CharField('Name', max_length=256)
    platform = models.CharField('Platform', max_length=256)
    short = models.CharField('Platform shortname', max_length=10, blank=True, default="")
    markdown = models.TextField('Description', help_text='Use Markdown syntax', blank=True, default="")
    html = models.TextField('Description HTML', default="", blank=True)
    launch = models.DateTimeField('Launch Date')
    end = models.DateTimeField('Mission Ended', blank=True, null=True)
    url = models.URLField('Data Access URL', blank=True, max_length=1024, default="")
    logo = models.ImageField('Logo', max_length=512, upload_to='images/missions', blank=True, default="")

    def __str__(self):

        return self.name


# archive class
class Archive(models.Model):

    # set archive parameters
    number = models.IntegerField('Number', default=0, unique=True)
    description = models.TextField('Description', blank=True)

    def __str__(self):

        return str(self.number)


# measurement class
class Measurement(models.Model):

    # set file types
    types = (('data', 'Data'), ('image', 'Image'))

    # set attributes
    name = models.CharField('Name', max_length=500)
    description = models.TextField('Description', blank=True)
    archives = models.ManyToManyField(Archive)
    archive = models.ForeignKey(Archive, to_field='number', default=6400, related_name='measTypes', on_delete=models.DO_NOTHING)
    availability = models.DateTimeField('Availability Start Time', default=datetime(1840, 1, 1))
    sensor = models.ForeignKey(Sensor, blank=True, null=True, on_delete=models.DO_NOTHING)

    # Average size of products in MB.
    size = models.DecimalField('Average file size', max_digits=11, decimal_places=2, default=1.00, blank=False)
    type = models.CharField('File Type [optional]', max_length=5, blank=True, choices=types)
    url = models.URLField('Sample Image [URL, optional]', blank=True, max_length=1024, default="")
    acps = models.BooleanField('On ACPS?', default=True, blank=False)
    evil = models.CharField('Evil String', help_text="Files matching this pattern won't be imported.", default="", blank=True, max_length=500)
    internal = models.BooleanField('Is this a non-public dataset?', default=False)
    updated = models.DateTimeField('Last Successful Update', default=datetime(1840, 1, 1))
    timeout = models.IntegerField('Timeout for indexing/importing from external sources', help_text="[seconds]", default=60)

    def __str__(self):

        return str(self.name)


# Configuration Model
class Configuration(models.Model):

    # add configuration attributes
    name = models.CharField("Name", max_length=100, default="OMPS Trending Configuration", unique=True)

    # add string method
    def __str__(self):

        return self.name


# Product Model
class Product(models.Model):

    # add product attributes
    configuration = models.ForeignKey(Configuration, related_name="trending_products", null=True, on_delete=models.DO_NOTHING)
    measurement = models.OneToOneField(Measurement, unique=True, on_delete=models.DO_NOTHING)
    available = models.DateTimeField("Last Available Date", default = datetime(1840, 1, 1))
    refreshed = models.DateTimeField("Last Refreshed Date", default = datetime(1840, 1, 1))
    archive = models.ForeignKey(Archive, to_field='number', default=60000, on_delete=models.DO_NOTHING)
    file = models.FileField("Latest File", upload_to="trending", max_length=1024, blank=True, default="")
    orbit = models.IntegerField("Latest Orbit Number", default=0)

    def __str__(self):

        return str(self.measurement)


# Parameter Model
class Parameter(models.Model):

    # set plot types
    types = (('markers', 'Markers'), ('lines', 'Lines'), ('lines_and_markers', 'Lines + Markers'))

    # add parameter attributes
    configuration = models.ForeignKey(Configuration, related_name="trending_parameters", null=True, on_delete=models.DO_NOTHING)
    name = models.CharField('Name', max_length=256, default='')
    product = models.ForeignKey(Product, related_name="parameters", on_delete=models.DO_NOTHING, default=1)
    data = models.TextField("JSON", help_text='JSON formatted trending data', default="{}")
    sparkline = models.TextField("Sparkline Data URI", max_length=2000, default="")

    # add plot attributes
    type = models.CharField('Plot Type', max_length=20, choices=types, default='lines')
    allow = models.BooleanField("Allow this to be plotted?", default=False)
    orbits = models.ForeignKey("self", related_name="+", null=True, blank=True, on_delete=models.DO_NOTHING)
    dates = models.ForeignKey("self", related_name="+", null=True, blank=True, on_delete=models.DO_NOTHING)
    independents = models.ManyToManyField("self", related_name="+", null=True, blank=True)
    fill = models.CharField("Fill Value", max_length=1024, default="", blank=True, null=True)
    long = models.CharField('Long Name', max_length=1024, default="")
    legend = models.CharField('Plot Legend', max_length=10000, default="[]")
    units = models.CharField('Units', max_length=50, default="")
    status = models.CharField('Status', max_length=150, default="", blank=True)
    abscissa = models.CharField('Default Independent Variable', max_length=1024, default="")

    # add string method
    def __str__(self):

        return self.name


# Category Model
class Category(models.Model):

    # add category attributes
    configuration = models.ForeignKey(Configuration, related_name="trending_categories", null=True, on_delete=models.DO_NOTHING)
    name = models.CharField('Category Name', max_length=512, default="")
    path = models.CharField('Full Path', max_length=512, blank=False, unique=True)
    nest = models.IntegerField('Nesting Level', default=0)
    parameters = models.ManyToManyField(Parameter, related_name='categories')

    # add string method
    def __str__(self):

        return self.name


# User Model
class Preferences(models.Model):

    # add user attributes
    user = models.ForeignKey(User, unique=True, on_delete=models.DO_NOTHING)

    def __str__(self):

        return self.user.name


# Profile Model
class Profile(models.Model):

    # add profile attributes
    base = models.ForeignKey(Preferences, related_name="trending_filters", on_delete=models.DO_NOTHING)
    name = models.CharField("Config Name", max_length=1024, blank=False)
    categories = models.ManyToManyField(Category, blank=True)
    parameters = models.ManyToManyField(Parameter, blank=True)
    products = models.ManyToManyField(Product, blank=True)
    status = models.BooleanField("User's default filter?", default=False)

    # add string method
    def __str__(self):

        return self.name