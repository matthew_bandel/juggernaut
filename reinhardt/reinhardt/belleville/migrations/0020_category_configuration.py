# Generated by Django 2.2.5 on 2021-05-04 21:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('belleville', '0019_auto_20210504_2113'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='configuration',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='trending_categories', to='belleville.Configuration'),
        ),
    ]
