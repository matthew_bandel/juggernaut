# Generated by Django 2.2.5 on 2021-05-04 05:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('belleville', '0007_auto_20210504_0515'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='parameter',
            name='legend',
        ),
    ]
