# Generated by Django 2.2.5 on 2021-05-04 19:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('belleville', '0014_remove_parameter_product'),
    ]

    operations = [
        migrations.AddField(
            model_name='parameter',
            name='product',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, related_name='parameters', to='belleville.Product'),
        ),
    ]
