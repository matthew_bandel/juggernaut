# Generated by Django 2.2.5 on 2021-05-04 06:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('belleville', '0009_parameter_legend'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='parameter',
            name='status',
        ),
    ]
