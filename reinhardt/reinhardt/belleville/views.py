from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

# import models
from belleville.models import Parameter

# import django shortcuts
from django.shortcuts import render, render_to_response

# import bokeh
from bokeh.resources import CDN
from bokeh.plotting import figure, show
from bokeh.embed import components

# evaluate literals
from ast import literal_eval


# create opening view
def homepage(request):

    # begin scripts and divs
    scripts = ''
    divs = ''

    # get all parameters
    parameters = Parameter.objects.all()
    for parameter in parameters:

        # add name to divs
        divs += '<p>{}</p>'.format(parameter.name)

        # add sparkline
        divs += '<img id = "ItemPreview", src = "{}" </img>'.format(parameter.sparkline)

        # get orbitnumber
        orbits, creation = Parameter.objects.get_or_create(name='OrbitNumber')

        # get data
        x = [float(entry) for entry in literal_eval(orbits.data)[0]]
        y = [float(entry) for entry in literal_eval(parameter.data)[0]]

        # plot figure
        plot = figure()
        plot.line(x, y)

        # get components
        script, div = components(plot, CDN)

        # add to scripts and divs
        scripts += script
        divs += div

    # create response
    response = render(request, 'base.html', {'div': divs, 'script': scripts})

    return response
