# import system tools
import os
import json

# import math function
from math import ceil, floor, sqrt

# import h5py to read h5 files
import h5py

# import Pillow and io for making sparklines
from PIL import Image, ImageDraw
from io import StringIO, BytesIO
from urllib.parse import quote

# import django
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

# import belleville models
from belleville.models import Sensor, Archive, Measurement, Configuration
from belleville.models import Parameter, Category, Product, Preferences, Profile


# class Command to direct commandline arguments
class Command(BaseCommand):

    # set help attribute
    help = 'Extract the given directory of files'

    # initiate other attributes
    product = None
    profile = None
    configuration = None

    def _calendar(self):
        """Get the orbit number and dates information from the hdf5 file.

        Arguments:
            five: hdf5 file

        Returns:
            None
        """

        # get default configuration and product
        configuration = self.configuration
        product = self.product

        # create slots for orbit number
        name = 'OrbitNumber'
        parameter, creation = Parameter.objects.get_or_create(name=name, configuration=configuration, product=product)
        parameter.save()

        # create entry for year fraction
        name = 'OrbitStartTimeFrYr'
        parameter, creation = Parameter.objects.get_or_create(name=name, configuration=configuration, product=product)
        parameter.save()

        return None

    def _categorize(self, chain):
        """Extract categories from h5 file.

        Arguments:
            chain: list of fields

        Returns:
            None
        """

        # parse the chain for attributes
        name = chain[-1]

        # print status
        self.stdout.write('creating category {}...'.format(name))

        # get the default configuration
        configuration = self.configuration

        # add the entry to the database
        category, creation = Category.objects.get_or_create(name=name, configuration=configuration)

        # update other attributes
        category.nest = len(chain)
        category.path = '/'.join(chain)

        # save category
        category.save()

        # add category to profile
        profile = self.profile
        profile.categories.add(category)
        profile.save()

        return None

    def _establish(self):
        """Establish default profile, product, and configuration.

        Arguments:
            None

        Returns:
            None
        """

        # get base preferences
        user, creation = User.objects.get_or_create(first_name='Stefano')
        base, creation = Preferences.objects.get_or_create(user=user)

        # get profile instance
        profile, creation = Profile.objects.get_or_create(name='OMI_Profile', base=base)

        # get configuration, measurement, and archive
        configuration, creation = Configuration.objects.get_or_create(name='OMI_Trending_Configuration')
        measurement, creation = Measurement.objects.get_or_create(name='OMI')
        archive, creation = Archive.objects.get_or_create(description='OMI_Archive')

        # get matching product instance
        product, creation = Product.objects.get_or_create(configuration=configuration, measurement=measurement, archive=archive)

        # set defaults
        self.product = product
        self.profile = profile
        self.configuration = configuration

        return None

    def _extract(self, five, chain=None):
        """Extract categories and parameters from hdf5 file or group.

        Arguments:
            five: hdf5 file or group=
            chain: list of str, route so far

        Returns:
            None
        """

        # default chain to empty
        chain = chain or []

        # try to
        try:

            # add as a dataset to the Parameters table
            array = five[:]
            attributes = dict(five.attrs)
            self._parameterize(array, chain, attributes)

        # otherwise assume a group
        except AttributeError:

            # for a nonempty chain
            if chain:

                # add to the Categories table
                self._categorize(chain)

            # and continue recursively
            for field in list(five.keys()):

                # and continue recursively
                self._extract(five[field], chain + [field])

        return None

    def _parameterize(self, array, chain, attributes):
        """Extract parameters from h5 file.

        Arguments:
            array: hdf5 file dataset
            chain: list of str, previous chain of fields
            attributes: dict, dataset attributes

        Returns:
            None
        """

        # get the name as the last entry
        name = chain[-1]

        # print status
        self.stdout.write('creating parameter {}...'.format(name))

        # get configuration and product
        configuration = self.configuration
        product = self.product

        # create parameter or grab by name
        parameter, creation = Parameter.objects.get_or_create(name=name, configuration=configuration, product=product)

        # create the data as a list for json formatting
        data = [list(array)]

        # generate a sparkline
        sparkline = self._spark(array)

        # set orbits and dates
        orbits = Parameter.objects.get(name="OrbitNumber")
        dates = Parameter.objects.get(name="OrbitStartTimeFrYr")

        # update data attributes
        parameter.data = data
        parameter.sparkline = sparkline

        # update abscissa references
        parameter.orbits = orbits
        parameter.dates = dates

        # add other attributes
        parameter.fill = attributes['FillValue']
        parameter.long = attributes['LongName']
        parameter.units = attributes['Units']
        parameter.status = attributes['Status']
        parameter.legend = json.dumps([attributes['PlotLegend']])
        parameter.abscissa = json.dumps([attributes['AbscissaVariableSelection']])

        # add orbit number
        name = 'OrbitNumber'
        independent, creation = Parameter.objects.get_or_create(name=name, configuration=configuration, product=product)
        parameter.independents.add(independent)

        # add year fractions
        name = 'OrbitStartTimeFrYr'
        independent, creation = Parameter.objects.get_or_create(name=name, configuration=configuration, product=product)
        parameter.independents.add(independent)

        # save the parameter
        parameter.save()

        # add the category to the parameter table
        category = Category.objects.get(path='/'.join(chain[:-1]), configuration=configuration)
        category.parameters.add(parameter)
        category.save()

        # add the parameter to the profle
        profile = self.profile
        profile.parameters.add(parameter)
        profile.save()

        return None

    def _spark(self, data):
        """Create sparkline from 1-D data.

        Arguments:
            data: hdf5 or numpy array

        Returns:
            sparkline
        """

        # set image attributes in pixels
        width, height = (200, 20)

        # begin image
        image = Image.new("RGB", (width, height), 'white')

        # determine indices for subsampling based on image width
        chunk = len(data) / width
        indices = [floor(index * chunk) for index in range(width)]
        indices = list(set(indices))
        indices.sort()

        # subsample the data,
        sample = data[indices]

        # convert to float and flip the y-axis (PIL images y-axis increases towards bottom)
        sample = [float(datum) * -1 for datum in sample]

        # determine extent of y range
        maximum = max(sample)
        minimum = min(sample)
        extent = maximum - minimum

        # calculate a margin and data range
        margin = max([0.1 * extent, 0.001])
        top = maximum + margin
        bottom = minimum - margin

        # in case top and bottom are fills, change gap to 1.0
        gap = (top - bottom) or 1.0

        # translate the data to pixel heights
        pixels = [height * (datum - bottom) / gap for datum in sample]
        points = [(horizontal, vertical) for horizontal, vertical in enumerate(pixels)]

        # make artist and draw the line
        artist = ImageDraw.Draw(image)

        # draw the line in gray
        gray = "#888888"
        artist.line(points, fill=gray)

        # draw a red rectangle at the end
        red = "#FF0000"
        rectangle = [len(sample) - 2, pixels[-1] - 1, len(sample), pixels[-1] + 1]
        artist.rectangle(rectangle, fill=red)

        # create file pointer as bytes
        pointer = BytesIO()
        image.save(pointer, "png")

        # craete sparkline
        sparkline = "data:image/png,{}".format(quote(pointer.getvalue()))

        return sparkline

    def add_arguments(self, parser):

        # parse the arguments
        parser.add_argument('directory')

        return None

    def handle(self, *args, **options):
        """Handle the extract command.

        Arguments:
            *args: unpacked list of args
            **options: unpacked dictiory

        Returns:
            None
        """

        # establish default configurations, profiles, etc
        self._establish()

        # get directory
        directory = options['directory']

        # get all paths
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        # go through each path
        for path in paths:

            # print
            self.stdout.write('extracting {}...'.format(path))

            # read in  hdf5 file
            five = h5py.File(path, 'r')

            # create dummy abscissa references
            self._calendar()

            # extract independent variables, using it as the category name
            self._extract(five['IndependentVariables'], ['IndependentVariables'])

            # extract categories and parameters, ignoring the Categories node
            self._extract(five['Categories'])

            # close file
            five.close()

        return None
