# import system tools
import os

# import math function
from math import ceil, floor, sqrt

# import datetime
from datetime import datetime

# import h5py to read h5 files
import h5py

# import django
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

# import belleville models
from belleville.models import Sensor, Archive, Measurement, Configuration
from belleville.models import Product, Preferences, Profile


# class Command to direct commandline arguments
class Command(BaseCommand):

    # set help attribute
    help = 'Establish larger project structure'

    # archive class
    def _archive(self):
        """Establish default archive model.

        Arguments:
            None

        Returns:
            None
        """

        # set attributes
        number = 0
        description = 'OMI_Archive'

        # create
        archive, creation = Archive.objects.get_or_create(number=number, description=description)

        # save
        archive.save()

        return None

    def _configure(self):
        """Establish default configuration instance.

        Arguments:
            None

        Returns:
            None
        """

        # create configuration instance
        name = 'OMI_Trending_Configuration'
        configuration, creation = Configuration.objects.get_or_create(name=name)

        # save
        configuration.save()

        return None

    def _measure(self):
        """Establish default measurement instance.

        Arguments:
            None

        Returns:
            None
        """

        # define measurement and get archive
        name = 'OMI'
        archive, creation = Archive.objects.get_or_create(description='OMI_Archive')

        # create instance
        measurement, creation = Measurement.objects.get_or_create(name=name, archive=archive)

        # add archive to archives
        measurement.archives.add(archive)

        # add sensor
        sensor, creation = Sensor.objects.get_or_create(name='Ozone_Monitoring_Instrument')
        measurement.sensor = sensor

        # add other attributes
        measurement.description = 'OMI_measurements'
        measurement.availability = datetime(2005, 1, 1)
        measurement.size = 1.0
        measurement.type = 'hdf5'
        measurement.url = ''
        measurement.acps = True
        measurement.evil = 'evil string'
        measurement.internal = False
        measurement.updated = datetime(2021, 1, 1)
        measurement.timeout = 60

        # save measurement
        measurement.save()

        return None

    def _prefer(self):
        """Establish default user preferences.

        Arguments:
            None

        Returns:
            None
        """

        # create user
        user, creation = User.objects.get_or_create(first_name='Stefano')

        # create preferences
        preferences, creation = Preferences.objects.get_or_create(user=user)
        preferences.save()

        return None

    def _profile(self):
        """Establish default profile.

        Arguments:
            None

        Returns:
            None
        """

        # get base preferences
        user, creation = User.objects.get_or_create(first_name='Stefano')
        base, creation = Preferences.objects.get_or_create(user=user)

        # create instance
        name = 'OMI_Profile'
        profile, creation = Profile.objects.get_or_create(name=name, base=base)

        # add default status
        profile.status = False

        # get configuration, measurement, and archive
        configuration, creation = Configuration.objects.get_or_create(name='OMI_Trending_Configuration')
        measurement, creation = Measurement.objects.get_or_create(name='OMI')
        archive, creation = Archive.objects.get_or_create(description='OMI_Archive')

        # get matching product instance
        product, creation = Product.objects.get_or_create(configuration=configuration, measurement=measurement, archive=archive)

        # add to profile
        profile.products.add(product)

        # save profile
        profile.save()

        return None

    def _produce(self):
        """Establish default product instance.

        Arguments:
            None

        Returns:
            None
        """

        # get configuration, measurement, and archive
        configuration, creation = Configuration.objects.get_or_create(name='OMI_Trending_Configuration')
        measurement, creation = Measurement.objects.get_or_create(name='OMI')
        archive, creation = Archive.objects.get_or_create(description='OMI_Archive')

        # create instance
        product, creation = Product.objects.get_or_create(configuration=configuration, measurement=measurement, archive=archive)

        # add product attributes
        product.available = datetime(2005, 1, 1)
        product.refreshed = datetime(2021, 1, 1)
        product.file = ''
        product.orbit = 30000

        # save
        product.save()

        return None

    def _sense(self):
        """Establish sensor instance.

        Arguments:
            None

        Returns:
            None
        """

        # set name and launch date
        name = 'Ozone_Monitoring_Instrument'
        launch = datetime(2005, 1, 1)

        # create sensor
        sensor, creation = Sensor.objects.get_or_create(name=name, launch=launch)

        # add other attributes
        sensor.platform = 'MacOS'
        sensor.short = 'OMI'
        sensor.markdown = 'ozone monitoring instrument'
        sensor.html = '<p>ozone monitoring instrument<p/>'
        sensor.end = datetime(2021, 1, 1)
        sensor.url = ''
        sensor.logo = ''

        # save sensor
        sensor.save()

        return None

    def handle(self, *args, **options):
        """Handle the extract command.

        Arguments:
            *args: unpacked list of args
            **options: unpacked dictiory

        Returns:
            None
        """

        # establish sensor
        self._sense()

        # establish archive
        self._archive()

        # establish measurement class
        self._measure()

        # establish default configuration
        self._configure()

        # establish default product
        self._produce()

        # establish default user preferences
        self._prefer()

        # establish default profile
        self._profile()

        return None
