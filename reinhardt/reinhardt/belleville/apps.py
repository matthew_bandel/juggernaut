from django.apps import AppConfig


class BellevilleConfig(AppConfig):
    name = 'belleville'
