--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_profile_parameters; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_profile_parameters (
    id integer NOT NULL,
    profile_id integer NOT NULL,
    parameter_id integer NOT NULL
);


ALTER TABLE public.belleville_profile_parameters OWNER TO matthewbandel;

--
-- Name: belleville_profile_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_profile_parameters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_profile_parameters_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_profile_parameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_profile_parameters_id_seq OWNED BY public.belleville_profile_parameters.id;


--
-- Name: belleville_profile_parameters id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_parameters ALTER COLUMN id SET DEFAULT nextval('public.belleville_profile_parameters_id_seq'::regclass);


--
-- Data for Name: belleville_profile_parameters; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_profile_parameters (id, profile_id, parameter_id) FROM stdin;
130	1	2357
131	1	2359
132	1	2360
133	1	2361
134	1	2362
135	1	2363
136	1	2364
137	1	2358
138	1	2365
139	1	2366
140	1	2367
141	1	2368
142	1	2369
143	1	2370
144	1	2371
145	1	2372
146	1	2373
147	1	2374
148	1	2375
149	1	2376
150	1	2377
151	1	2378
152	1	2379
153	1	2380
154	1	2381
155	1	2382
156	1	2383
157	1	2384
158	1	2385
159	1	2386
160	1	2387
161	1	2388
162	1	2389
163	1	2390
164	1	2391
165	1	2392
166	1	2393
167	1	2394
168	1	2395
169	1	2396
170	1	2397
171	1	2398
172	1	2399
173	1	2400
174	1	2401
175	1	2402
176	1	2403
177	1	2404
178	1	2405
179	1	2406
180	1	2407
181	1	2408
182	1	2409
183	1	2410
184	1	2411
185	1	2412
186	1	2413
187	1	2414
188	1	2415
189	1	2416
190	1	2417
191	1	2418
192	1	2419
193	1	2420
194	1	2421
195	1	2422
196	1	2423
197	1	2424
198	1	2425
199	1	2426
200	1	2427
201	1	2428
202	1	2429
203	1	2430
204	1	2431
205	1	2432
206	1	2433
207	1	2434
208	1	2435
209	1	2436
210	1	2437
211	1	2438
212	1	2439
213	1	2440
214	1	2441
215	1	2442
216	1	2443
217	1	2444
218	1	2445
219	1	2446
220	1	2447
221	1	2448
222	1	2449
223	1	2450
224	1	2451
225	1	2452
226	1	2453
227	1	2454
228	1	2455
229	1	2456
230	1	2457
231	1	2458
232	1	2459
233	1	2460
234	1	2461
235	1	2462
236	1	2463
237	1	2464
238	1	2465
239	1	2466
240	1	2467
241	1	2468
242	1	2469
243	1	2470
244	1	2471
245	1	2472
246	1	2473
247	1	2474
248	1	2475
249	1	2476
250	1	2477
251	1	2478
252	1	2479
253	1	2480
254	1	2481
255	1	2482
256	1	2483
257	1	2484
258	1	2485
\.


--
-- Name: belleville_profile_parameters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_profile_parameters_id_seq', 258, true);


--
-- Name: belleville_profile_parameters belleville_profile_param_profile_id_parameter_id_e8fb6ba3_uniq; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_parameters
    ADD CONSTRAINT belleville_profile_param_profile_id_parameter_id_e8fb6ba3_uniq UNIQUE (profile_id, parameter_id);


--
-- Name: belleville_profile_parameters belleville_profile_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_parameters
    ADD CONSTRAINT belleville_profile_parameters_pkey PRIMARY KEY (id);


--
-- Name: belleville_profile_parameters_parameter_id_a796b5ef; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_profile_parameters_parameter_id_a796b5ef ON public.belleville_profile_parameters USING btree (parameter_id);


--
-- Name: belleville_profile_parameters_profile_id_99ba1a2e; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_profile_parameters_profile_id_99ba1a2e ON public.belleville_profile_parameters USING btree (profile_id);


--
-- Name: belleville_profile_parameters belleville_profile_p_parameter_id_a796b5ef_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_parameters
    ADD CONSTRAINT belleville_profile_p_parameter_id_a796b5ef_fk_bellevill FOREIGN KEY (parameter_id) REFERENCES public.belleville_parameter(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: belleville_profile_parameters belleville_profile_p_profile_id_99ba1a2e_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_parameters
    ADD CONSTRAINT belleville_profile_p_profile_id_99ba1a2e_fk_bellevill FOREIGN KEY (profile_id) REFERENCES public.belleville_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

