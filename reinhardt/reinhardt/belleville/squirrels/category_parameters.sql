--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_category_parameters; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_category_parameters (
    id integer NOT NULL,
    category_id integer NOT NULL,
    parameter_id integer NOT NULL
);


ALTER TABLE public.belleville_category_parameters OWNER TO matthewbandel;

--
-- Name: belleville_category_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_category_parameters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_category_parameters_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_category_parameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_category_parameters_id_seq OWNED BY public.belleville_category_parameters.id;


--
-- Name: belleville_category_parameters id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category_parameters ALTER COLUMN id SET DEFAULT nextval('public.belleville_category_parameters_id_seq'::regclass);


--
-- Data for Name: belleville_category_parameters; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_category_parameters (id, category_id, parameter_id) FROM stdin;
1584	207	2357
1585	207	2359
1586	207	2360
1587	207	2361
1588	207	2362
1589	207	2363
1590	207	2364
1591	207	2358
1592	207	2365
1593	212	2366
1594	212	2367
1595	212	2368
1596	212	2369
1597	212	2370
1598	212	2371
1599	212	2372
1600	212	2373
1601	212	2374
1602	212	2375
1603	212	2376
1604	212	2377
1605	212	2378
1606	212	2379
1607	212	2380
1608	212	2381
1609	212	2382
1610	212	2383
1611	212	2384
1612	212	2385
1613	212	2386
1614	212	2387
1615	212	2388
1616	212	2389
1617	212	2390
1618	212	2391
1619	212	2392
1620	212	2393
1621	212	2394
1622	212	2395
1623	212	2396
1624	212	2397
1625	212	2398
1626	212	2399
1627	212	2400
1628	212	2401
1629	212	2402
1630	212	2403
1631	212	2404
1632	212	2405
1633	212	2406
1634	212	2407
1635	212	2408
1636	212	2409
1637	212	2410
1638	212	2411
1639	212	2412
1640	212	2413
1641	212	2414
1642	212	2415
1643	212	2416
1644	212	2417
1645	212	2418
1646	212	2419
1647	212	2420
1648	212	2421
1649	212	2422
1650	212	2423
1651	212	2424
1652	212	2425
1653	212	2426
1654	212	2427
1655	212	2428
1656	212	2429
1657	212	2430
1658	212	2431
1659	212	2432
1660	212	2433
1661	212	2434
1662	212	2435
1663	212	2436
1664	212	2437
1665	212	2438
1666	212	2439
1667	212	2440
1668	212	2441
1669	212	2442
1670	212	2443
1671	212	2444
1672	212	2445
1673	214	2446
1674	214	2447
1675	214	2448
1676	214	2449
1677	214	2450
1678	214	2451
1679	214	2452
1680	214	2453
1681	214	2454
1682	214	2455
1683	214	2456
1684	214	2457
1685	214	2458
1686	214	2459
1687	214	2460
1688	214	2461
1689	214	2462
1690	214	2463
1691	214	2464
1692	214	2465
1693	214	2466
1694	214	2467
1695	214	2468
1696	214	2469
1697	214	2470
1698	214	2471
1699	214	2472
1700	214	2473
1701	214	2474
1702	214	2475
1703	214	2476
1704	214	2477
1705	214	2478
1706	214	2479
1707	214	2480
1708	214	2481
1709	214	2482
1710	214	2483
1711	214	2484
1712	214	2485
\.


--
-- Name: belleville_category_parameters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_category_parameters_id_seq', 1712, true);


--
-- Name: belleville_category_parameters belleville_category_para_category_id_parameter_id_46b6ec74_uniq; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category_parameters
    ADD CONSTRAINT belleville_category_para_category_id_parameter_id_46b6ec74_uniq UNIQUE (category_id, parameter_id);


--
-- Name: belleville_category_parameters belleville_category_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category_parameters
    ADD CONSTRAINT belleville_category_parameters_pkey PRIMARY KEY (id);


--
-- Name: belleville_category_parameters_category_id_bacf2b79; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_category_parameters_category_id_bacf2b79 ON public.belleville_category_parameters USING btree (category_id);


--
-- Name: belleville_category_parameters_parameter_id_ae333d9f; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_category_parameters_parameter_id_ae333d9f ON public.belleville_category_parameters USING btree (parameter_id);


--
-- Name: belleville_category_parameters belleville_category__category_id_bacf2b79_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category_parameters
    ADD CONSTRAINT belleville_category__category_id_bacf2b79_fk_bellevill FOREIGN KEY (category_id) REFERENCES public.belleville_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: belleville_category_parameters belleville_category__parameter_id_ae333d9f_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category_parameters
    ADD CONSTRAINT belleville_category__parameter_id_ae333d9f_fk_bellevill FOREIGN KEY (parameter_id) REFERENCES public.belleville_parameter(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

