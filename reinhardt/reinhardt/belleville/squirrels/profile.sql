--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_profile; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_profile (
    id integer NOT NULL,
    name character varying(1024) NOT NULL,
    status boolean NOT NULL,
    base_id integer NOT NULL
);


ALTER TABLE public.belleville_profile OWNER TO matthewbandel;

--
-- Name: belleville_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_profile_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_profile_id_seq OWNED BY public.belleville_profile.id;


--
-- Name: belleville_profile id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile ALTER COLUMN id SET DEFAULT nextval('public.belleville_profile_id_seq'::regclass);


--
-- Data for Name: belleville_profile; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_profile (id, name, status, base_id) FROM stdin;
1	OMI_Profile	f	1
\.


--
-- Name: belleville_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_profile_id_seq', 1, true);


--
-- Name: belleville_profile belleville_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile
    ADD CONSTRAINT belleville_profile_pkey PRIMARY KEY (id);


--
-- Name: belleville_profile_base_id_8c380a30; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_profile_base_id_8c380a30 ON public.belleville_profile USING btree (base_id);


--
-- Name: belleville_profile belleville_profile_base_id_8c380a30_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile
    ADD CONSTRAINT belleville_profile_base_id_8c380a30_fk_bellevill FOREIGN KEY (base_id) REFERENCES public.belleville_preferences(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

