--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_configuration; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_configuration (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.belleville_configuration OWNER TO matthewbandel;

--
-- Name: belleville_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_configuration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_configuration_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_configuration_id_seq OWNED BY public.belleville_configuration.id;


--
-- Name: belleville_configuration id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_configuration ALTER COLUMN id SET DEFAULT nextval('public.belleville_configuration_id_seq'::regclass);


--
-- Data for Name: belleville_configuration; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_configuration (id, name) FROM stdin;
1	OMI_Trending_Configuration
\.


--
-- Name: belleville_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_configuration_id_seq', 1, true);


--
-- Name: belleville_configuration belleville_configuration_name_key; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_configuration
    ADD CONSTRAINT belleville_configuration_name_key UNIQUE (name);


--
-- Name: belleville_configuration belleville_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_configuration
    ADD CONSTRAINT belleville_configuration_pkey PRIMARY KEY (id);


--
-- Name: belleville_configuration_name_ad176ebc_like; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_configuration_name_ad176ebc_like ON public.belleville_configuration USING btree (name varchar_pattern_ops);


--
-- PostgreSQL database dump complete
--

