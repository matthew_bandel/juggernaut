--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_product; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_product (
    id integer NOT NULL,
    available timestamp with time zone NOT NULL,
    refreshed timestamp with time zone NOT NULL,
    file character varying(1024) NOT NULL,
    orbit integer NOT NULL,
    archive_id integer NOT NULL,
    configuration_id integer,
    measurement_id integer NOT NULL
);


ALTER TABLE public.belleville_product OWNER TO matthewbandel;

--
-- Name: belleville_product_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_product_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_product_id_seq OWNED BY public.belleville_product.id;


--
-- Name: belleville_product id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_product ALTER COLUMN id SET DEFAULT nextval('public.belleville_product_id_seq'::regclass);


--
-- Data for Name: belleville_product; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_product (id, available, refreshed, file, orbit, archive_id, configuration_id, measurement_id) FROM stdin;
1	2004-12-31 16:00:00-08	2020-12-31 16:00:00-08		30000	0	1	6
\.


--
-- Name: belleville_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_product_id_seq', 1, true);


--
-- Name: belleville_product belleville_product_measurement_id_key; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_product
    ADD CONSTRAINT belleville_product_measurement_id_key UNIQUE (measurement_id);


--
-- Name: belleville_product belleville_product_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_product
    ADD CONSTRAINT belleville_product_pkey PRIMARY KEY (id);


--
-- Name: belleville_product_archive_id_23e578e0; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_product_archive_id_23e578e0 ON public.belleville_product USING btree (archive_id);


--
-- Name: belleville_product_configuration_id_12af4623; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_product_configuration_id_12af4623 ON public.belleville_product USING btree (configuration_id);


--
-- Name: belleville_product belleville_product_archive_id_23e578e0_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_product
    ADD CONSTRAINT belleville_product_archive_id_23e578e0_fk_bellevill FOREIGN KEY (archive_id) REFERENCES public.belleville_archive(number) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: belleville_product belleville_product_configuration_id_12af4623_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_product
    ADD CONSTRAINT belleville_product_configuration_id_12af4623_fk_bellevill FOREIGN KEY (configuration_id) REFERENCES public.belleville_configuration(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: belleville_product belleville_product_measurement_id_9dfbe17a_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_product
    ADD CONSTRAINT belleville_product_measurement_id_9dfbe17a_fk_bellevill FOREIGN KEY (measurement_id) REFERENCES public.belleville_measurement(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

