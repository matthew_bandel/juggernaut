--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_profile_categories; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_profile_categories (
    id integer NOT NULL,
    profile_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.belleville_profile_categories OWNER TO matthewbandel;

--
-- Name: belleville_profile_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_profile_categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_profile_categories_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_profile_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_profile_categories_id_seq OWNED BY public.belleville_profile_categories.id;


--
-- Name: belleville_profile_categories id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_categories ALTER COLUMN id SET DEFAULT nextval('public.belleville_profile_categories_id_seq'::regclass);


--
-- Data for Name: belleville_profile_categories; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_profile_categories (id, profile_id, category_id) FROM stdin;
10	1	207
11	1	208
12	1	209
13	1	210
14	1	211
15	1	212
16	1	213
17	1	214
\.


--
-- Name: belleville_profile_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_profile_categories_id_seq', 17, true);


--
-- Name: belleville_profile_categories belleville_profile_categ_profile_id_category_id_a625499f_uniq; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_categories
    ADD CONSTRAINT belleville_profile_categ_profile_id_category_id_a625499f_uniq UNIQUE (profile_id, category_id);


--
-- Name: belleville_profile_categories belleville_profile_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_categories
    ADD CONSTRAINT belleville_profile_categories_pkey PRIMARY KEY (id);


--
-- Name: belleville_profile_categories_category_id_ff27e137; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_profile_categories_category_id_ff27e137 ON public.belleville_profile_categories USING btree (category_id);


--
-- Name: belleville_profile_categories_profile_id_977f6b58; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_profile_categories_profile_id_977f6b58 ON public.belleville_profile_categories USING btree (profile_id);


--
-- Name: belleville_profile_categories belleville_profile_c_category_id_ff27e137_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_categories
    ADD CONSTRAINT belleville_profile_c_category_id_ff27e137_fk_bellevill FOREIGN KEY (category_id) REFERENCES public.belleville_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: belleville_profile_categories belleville_profile_c_profile_id_977f6b58_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_categories
    ADD CONSTRAINT belleville_profile_c_profile_id_977f6b58_fk_bellevill FOREIGN KEY (profile_id) REFERENCES public.belleville_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

