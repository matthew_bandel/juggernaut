--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_category; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_category (
    id integer NOT NULL,
    name character varying(512) NOT NULL,
    path character varying(512) NOT NULL,
    nest integer NOT NULL,
    configuration_id integer
);


ALTER TABLE public.belleville_category OWNER TO matthewbandel;

--
-- Name: belleville_category_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_category_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_category_id_seq OWNED BY public.belleville_category.id;


--
-- Name: belleville_category id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category ALTER COLUMN id SET DEFAULT nextval('public.belleville_category_id_seq'::regclass);


--
-- Data for Name: belleville_category; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_category (id, name, path, nest, configuration_id) FROM stdin;
207	IndependentVariables	IndependentVariables	1	1
208	L1-OML1BRUG	L1-OML1BRUG	1	1
209	BAND2_RADIANCE	L1-OML1BRUG/BAND2_RADIANCE	2	1
210	STANDARD_MODE	L1-OML1BRUG/BAND2_RADIANCE/STANDARD_MODE	3	1
211	INSTRUMENT	L1-OML1BRUG/BAND2_RADIANCE/STANDARD_MODE/INSTRUMENT	4	1
214	spectral_channel_quality	L1-OML1BRUG/BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality	5	1
212	housekeeping_data	L1-OML1BRUG/BAND2_RADIANCE/STANDARD_MODE/INSTRUMENT/housekeeping_data	5	1
213	OBSERVATIONS	L1-OML1BRUG/BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS	4	1
\.


--
-- Name: belleville_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_category_id_seq', 214, true);


--
-- Name: belleville_category belleville_category_path_key; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category
    ADD CONSTRAINT belleville_category_path_key UNIQUE (path);


--
-- Name: belleville_category belleville_category_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category
    ADD CONSTRAINT belleville_category_pkey PRIMARY KEY (id);


--
-- Name: belleville_category_configuration_id_472d1782; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_category_configuration_id_472d1782 ON public.belleville_category USING btree (configuration_id);


--
-- Name: belleville_category_path_f52a4835_like; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_category_path_f52a4835_like ON public.belleville_category USING btree (path varchar_pattern_ops);


--
-- Name: belleville_category belleville_category_configuration_id_472d1782_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_category
    ADD CONSTRAINT belleville_category_configuration_id_472d1782_fk_bellevill FOREIGN KEY (configuration_id) REFERENCES public.belleville_configuration(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

