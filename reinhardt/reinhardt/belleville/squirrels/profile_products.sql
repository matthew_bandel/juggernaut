--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: belleville_profile_products; Type: TABLE; Schema: public; Owner: matthewbandel
--

CREATE TABLE public.belleville_profile_products (
    id integer NOT NULL,
    profile_id integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE public.belleville_profile_products OWNER TO matthewbandel;

--
-- Name: belleville_profile_products_id_seq; Type: SEQUENCE; Schema: public; Owner: matthewbandel
--

CREATE SEQUENCE public.belleville_profile_products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belleville_profile_products_id_seq OWNER TO matthewbandel;

--
-- Name: belleville_profile_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthewbandel
--

ALTER SEQUENCE public.belleville_profile_products_id_seq OWNED BY public.belleville_profile_products.id;


--
-- Name: belleville_profile_products id; Type: DEFAULT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_products ALTER COLUMN id SET DEFAULT nextval('public.belleville_profile_products_id_seq'::regclass);


--
-- Data for Name: belleville_profile_products; Type: TABLE DATA; Schema: public; Owner: matthewbandel
--

COPY public.belleville_profile_products (id, profile_id, product_id) FROM stdin;
1	1	1
\.


--
-- Name: belleville_profile_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthewbandel
--

SELECT pg_catalog.setval('public.belleville_profile_products_id_seq', 1, true);


--
-- Name: belleville_profile_products belleville_profile_products_pkey; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_products
    ADD CONSTRAINT belleville_profile_products_pkey PRIMARY KEY (id);


--
-- Name: belleville_profile_products belleville_profile_products_profile_id_product_id_71138133_uniq; Type: CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_products
    ADD CONSTRAINT belleville_profile_products_profile_id_product_id_71138133_uniq UNIQUE (profile_id, product_id);


--
-- Name: belleville_profile_products_product_id_2368c69b; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_profile_products_product_id_2368c69b ON public.belleville_profile_products USING btree (product_id);


--
-- Name: belleville_profile_products_profile_id_080895b8; Type: INDEX; Schema: public; Owner: matthewbandel
--

CREATE INDEX belleville_profile_products_profile_id_080895b8 ON public.belleville_profile_products USING btree (profile_id);


--
-- Name: belleville_profile_products belleville_profile_p_product_id_2368c69b_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_products
    ADD CONSTRAINT belleville_profile_p_product_id_2368c69b_fk_bellevill FOREIGN KEY (product_id) REFERENCES public.belleville_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: belleville_profile_products belleville_profile_p_profile_id_080895b8_fk_bellevill; Type: FK CONSTRAINT; Schema: public; Owner: matthewbandel
--

ALTER TABLE ONLY public.belleville_profile_products
    ADD CONSTRAINT belleville_profile_p_profile_id_080895b8_fk_bellevill FOREIGN KEY (profile_id) REFERENCES public.belleville_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

