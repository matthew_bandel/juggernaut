#!/bin/bash

rm OMI/squirrels/*.sql

pg_dump --table '"OMI_trendingcategory_parameters"' omi >> "OMI/squirrels/OMI_trendingcategory_parameters.sql";
pg_dump --table '"OMI_trendingparameter_indvarChoices"' omi >> "OMI/squirrels/OMI_trendingparameter_indvarChoices.sql";
pg_dump --table '"OMI_trendingprofile_categories"' omi >> "OMI/squirrels/OMI_trendingprofile_categories.sql";
pg_dump --table '"OMI_trendingprofile_parameters"' omi >> "OMI/squirrels/OMI_trendingprofile_parameters.sql";
pg_dump --table '"OMI_trendingprofile_product"' omi >> "OMI/squirrels/OMI_trendingprofile_product.sql";
pg_dump --table '"OMI_trendingcategory"' omi >> "OMI/squirrels/OMI_trendingcategory.sql";
pg_dump --table '"OMI_trendingparameter"' omi >> "OMI/squirrels/OMI_trendingparameter.sql";
pg_dump --table '"OMI_trendingproduct"' omi >> "OMI/squirrels/OMI_trendingproduct.sql";
pg_dump --table '"OMI_trendingprofile"' omi >> "OMI/squirrels/OMI_trendingprofile.sql";
pg_dump --table '"OMI_userprofile"' omi >> "OMI/squirrels/OMI_userprofile.sql";
pg_dump --table '"OMI_trendingconfig"' omi >> "OMI/squirrels/OMI_trendingconfig.sql";

pg_dump --table '"data_measurementtype_archive_sets"' omi >> "OMI/squirrels/data_measurementtype_archive_sets.sql";
pg_dump --table '"data_measurementtype"' omi >> "OMI/squirrels/data_measurementtype.sql";
pg_dump --table '"data_archiveset"' omi >> "OMI/squirrels/data_archiveset.sql";
pg_dump --table '"data_sensor"' omi >> "OMI/squirrels/data_sensor.sql";

