/* run with command 'psql -d omi -f shiva.sql' */

/* delete omi tables */
delete from "OMI_trendingcategory_parameters";
delete from "OMI_trendingparameter_indvarChoices";
delete from "OMI_trendingprofile_categories";
delete from "OMI_trendingprofile_parameters";
delete from "OMI_trendingprofile_product";
delete from "OMI_trendingcategory";
delete from "OMI_trendingparameter";
delete from "OMI_trendingproduct";
delete from "OMI_trendingprofile";
delete from "OMI_userprofile";
delete from "OMI_trendingconfig";

/* delete data tables */
delete from "data_measurementtype_archive_sets";
delete from "data_measurementtype";
delete from "data_archiveset";
delete from "data_sensor";

