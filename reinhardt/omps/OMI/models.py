# import django models
from django.db import models
from django.contrib.auth.models import User

# import datetime
from datetime import datetime


# import data models
from data.models import Sensor, ArchiveSet, MeasurementType


# Configuration Model
class TrendingConfig(models.Model):

    # set name
    name = models.CharField("Name", max_length=100, default="OMI Trending Configuration", unique=True)


# Product Model
class TrendingProduct(models.Model):
    """Helps us manage trending files."""

    # set attributes
    config = models.ForeignKey(TrendingConfig, related_name="trending_products", null=True, on_delete=models.DO_NOTHING)
    measurementType = models.OneToOneField(MeasurementType, unique=True, on_delete=models.DO_NOTHING)
    lastAvailableDate = models.DateTimeField("Last Available Date", default = datetime(1840, 1, 1))
    lastRefreshedDate = models.DateTimeField("Last Refreshed Date", default = datetime(1840, 1, 1))
    archive_set = models.ForeignKey(ArchiveSet, to_field='number', default=0, on_delete=models.DO_NOTHING)
    latest_file = models.FileField("Latest File", upload_to="trending", max_length=1024, blank=True, default="")
    latest_orbit = models.IntegerField("Latest Orbit Number", default=0)

    def __unicode__(self):
        return u'%s' % (self.measurementType,)


# Trending Parameter Model
class TrendingParameter(models.Model):

    # add plot types
    PLOT_TYPE_CHOICES = (('markers', 'Markers'), ('lines', 'Lines'), ('lines_and_markers', 'Lines + Markers'))

    # add attributes
    config = models.ForeignKey(TrendingConfig, related_name="trending_parameters", null=True, on_delete=models.DO_NOTHING)
    name = models.CharField('Name', max_length=256)
    product = models.ForeignKey(TrendingProduct, related_name="parameters", on_delete=models.DO_NOTHING)
    json = models.TextField("JSON", help_text='JSON formatted trending data', default="{}")
    sparkline = models.TextField("Sparkline Data URI", max_length=2000, default="")
    plotType = models.CharField('Plot Type', max_length=20, choices=PLOT_TYPE_CHOICES, default='lines')
    doNotPlot = models.BooleanField("Allow this to be plotted?", default=False)
    orbits = models.ForeignKey("self", related_name="+", null=True, blank=True, on_delete=models.DO_NOTHING)
    dates = models.ForeignKey("self", related_name="+", null=True, blank=True, on_delete=models.DO_NOTHING)
    indvarChoices = models.ManyToManyField("self", related_name="+", null=True, blank=True)

    # ATTRIBUTES - NB: names match HDF attribute names
    FillValue = models.CharField("Fill Value", max_length=1024, default="", blank=True, null=True)
    LongName = models.CharField('Long Name', max_length=1024, default="")
    PlotLegend = models.CharField('Plot Legend', max_length=10000, default="[]")
    Units = models.CharField('Units', max_length=50, default="")
    Status = models.CharField('Status', max_length=150, default="", blank=True)
    AbscissaVariableSelection = models.CharField('Default Independent Variable', max_length=1024, default="")

    class Meta:
        verbose_name_plural = "Trending Parameters"
        unique_together = ('name', 'product',)

    def __unicode__(self):
        return u'%s - %s' % (self.name, self.product,)


# Category Model
class TrendingCategory(models.Model):

    # set attributes
    config = models.ForeignKey(TrendingConfig, related_name="trending_categories", null=True, on_delete=models.DO_NOTHING)
    name = models.CharField('Category Name', max_length=512, default="")
    full_path = models.CharField('Full Path', max_length=512, blank=False, unique=True)
    nesting_level = models.IntegerField('Nesting Level', default=0)
    parameters = models.ManyToManyField(TrendingParameter, related_name='categories')

    class Meta:
        verbose_name_plural = "Trending Categories"

    def __unicode__(self):
        return u'%s' % (self.name)


# User Model
class UserProfile(models.Model):

    # set attributes
    user = models.ForeignKey(User, unique=True, on_delete=models.DO_NOTHING)

    def __unicode__(self):
        return u'%s' % (self.user.username)


# Profile Model
class TrendingProfile(models.Model):

    # set attributes
    base_profile = models.ForeignKey(UserProfile, related_name="trending_filters", on_delete=models.DO_NOTHING)
    name = models.CharField("Config Name", max_length=1024, blank=False)
    categories = models.ManyToManyField(TrendingCategory, blank=True)
    parameters = models.ManyToManyField(TrendingParameter, blank=True)
    product = models.ManyToManyField(TrendingProduct, blank=True)
    isDefault = models.BooleanField("User's default filter?", default=False)

    class Meta:
        unique_together = ('base_profile', 'name',)

    def __unicode__(self):
        return u'%s' % (self.base_profile)

