from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

# import models
from OMI.models import TrendingParameter


# create opening view
def index(request):

    # begin opening text
    texts = ["<p>Hello, world. Welcome to OMI!</p>"]

    # get all parameters
    parameters = TrendingParameter.objects.all()
    for parameter in parameters:

        # add name to texts
        texts.append('<p>{}</p>'.format(parameter.name))

        # add image
        texts.append('<img id = "ItemPreview", src = "{}" </img>'.format(parameter.sparkline))

    # create response
    response = HttpResponse(''.join(texts))

    return response
