# import system tools
import os

# import math function
from math import ceil, floor, sqrt

# import datetime
from datetime import datetime

# import h5py to read h5 files
import h5py

# import django
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

# import OMI models
from data.models import Sensor, ArchiveSet, MeasurementType
from OMI.models import TrendingConfig, TrendingProduct, UserProfile, TrendingProfile


# class Command to direct commandline arguments
class Command(BaseCommand):

    # set help attribute
    help = 'Establish larger project structure'

    # archive class
    def _archive(self):
        """Establish default archive model.

        Arguments:
            None

        Returns:
            None
        """

        # set attributes
        number = 0
        description = 'OMI Archive'

        # create
        archive, creation = ArchiveSet.objects.get_or_create(number=number, description=description)

        # save
        archive.save()

        return None

    def _configure(self):
        """Establish default configuration instance.

        Arguments:
            None

        Returns:
            None
        """

        # create configuration instance
        name = 'OMI Trending Configuration'
        configuration, creation = TrendingConfig.objects.get_or_create(name=name)

        # save
        configuration.save()

        return None

    def _measure(self):
        """Establish default measurement instance.

        Arguments:
            None

        Returns:
            None
        """

        # define measurement and get archive
        name = 'OMI'
        archive, creation = ArchiveSet.objects.get_or_create(description='OMI Archive')

        # create instance
        measurement, creation = MeasurementType.objects.get_or_create(name=name, archive_set=archive)

        # add archive to archives
        measurement.archive_sets.add(archive)

        # add sensor
        sensor, creation = Sensor.objects.get_or_create(name='Ozone Monitoring Instrument')
        measurement.sensor = sensor

        # add other attributes
        measurement.description = 'OMI measurements'
        measurement.availability_start = datetime(2005, 1, 1)
        measurement.average_MB = 0.7
        measurement.file_type = 'hdf5'
        measurement.sample_url = ''
        measurement.on_acps = True
        measurement.evil_string = 'evil string'
        measurement.is_internal = False
        measurement.last_updated = datetime(2021, 5, 7)
        measurement.import_timeout = 60

        # save measurement
        measurement.save()

        return None

    def _prefer(self):
        """Establish default user preferences.

        Arguments:
            None

        Returns:
            None
        """

        # create user
        user, creation = User.objects.get_or_create(first_name='OMI')

        # create preferences
        preferences, creation = UserProfile.objects.get_or_create(user=user)
        preferences.save()

        return None

    def _produce(self):
        """Establish default product instance.

        Arguments:
            None

        Returns:
            None
        """

        # get configuration, measurement, and archive
        configuration, creation = TrendingConfig.objects.get_or_create(name='OMI Trending Configuration')
        measurement, creation = MeasurementType.objects.get_or_create(name='OMI')
        archive, creation = ArchiveSet.objects.get_or_create(description='OMI Archive')

        # create instance
        product, creation = TrendingProduct.objects.get_or_create(config=configuration, measurementType=measurement, archive_set=archive)

        # add product attributes
        product.lastAvailableDate = datetime(2005, 1, 1)
        product.lastRefreshedDate = datetime(2021, 1, 1)
        product.latest_file = ''
        product.latest_orbit = 30000

        # save
        product.save()

        return None

    def _profile(self):
        """Establish default profile.

        Arguments:
            None

        Returns:
            None
        """

        # get base preferences
        user, creation = User.objects.get_or_create(first_name='OMI')
        base, creation = UserProfile.objects.get_or_create(user=user)

        # create instance
        name = 'OMI Profile'
        profile, creation = TrendingProfile.objects.get_or_create(name=name, base_profile=base)

        # add default status
        profile.isDefault = False

        # get configuration, measurement, and archive
        configuration, creation = TrendingConfig.objects.get_or_create(name='OMI Trending Configuration')
        measurement, creation = MeasurementType.objects.get_or_create(name='OMI')
        archive, creation = ArchiveSet.objects.get_or_create(description='OMI Archive')

        # get matching product instance
        product, creation = TrendingProduct.objects.get_or_create(config=configuration, measurementType=measurement, archive_set=archive)

        # add to profile
        profile.product.add(product)

        # save profile
        profile.save()

        return None

    def _sense(self):
        """Establish sensor instance.

        Arguments:
            None

        Returns:
            None
        """

        # set name and launch date
        name = 'Ozone Monitoring Instrument'
        launch = datetime(2005, 1, 1)

        # create sensor
        sensor, creation = Sensor.objects.get_or_create(name=name, launch_date=launch)

        # add other attributes
        sensor.platform = 'EOS Aura'
        sensor.platform_shortname = 'Aura'
        sensor.description_markdown = 'ozone monitoring instrument'
        sensor.description_html = '<p>ozone monitoring instrument<p/>'
        sensor.mission_ended = datetime(2021, 1, 1)
        sensor.data_url = ''
        sensor.logo = ''

        # save sensor
        sensor.save()

        return None

    def handle(self, *args, **options):
        """Handle the extract command.

        Arguments:
            *args: unpacked list of args
            **options: unpacked dictiory

        Returns:
            None
        """

        # establish sensor
        self._sense()

        # establish archive
        self._archive()

        # establish measurement class
        self._measure()

        # establish default configuration
        self._configure()

        # establish default product
        self._produce()

        # establish default user preferences
        self._prefer()

        # establish default profile
        self._profile()

        return None
