# import system tools
import os
import json

# import datetime
from datetime import datetime

# import math function
from math import ceil, floor, sqrt

# import h5py to read h5 files
import h5py

# import Pillow and io for making sparklines
from PIL import Image, ImageDraw
from io import StringIO, BytesIO
from urllib.parse import quote

# import django
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

# import OMI models
from data.models import Sensor, ArchiveSet, MeasurementType
from OMI.models import TrendingConfig, TrendingProduct, UserProfile, TrendingProfile
from OMI.models import TrendingParameter, TrendingCategory, TrendingProduct


# class Command to direct commandline arguments
class Command(BaseCommand):

    # set help attribute
    help = 'Extract the given directory of files'

    # initiate other attributes
    product = None
    profile = None
    configuration = None

    def _calendar(self):
        """Get the orbit number and dates information from the hdf5 file.

        Arguments:
            five: hdf5 file

        Returns:
            None
        """

        # get default configuration and product
        configuration = self.configuration
        product = self.product

        # create slots for orbit number
        name = 'OrbitNumber'
        parameter, creation = TrendingParameter.objects.get_or_create(name=name, config=configuration, product=product)
        parameter.save()

        # create entry for year fraction
        name = 'OrbitStartTimeFrYr'
        parameter, creation = TrendingParameter.objects.get_or_create(name=name, config=configuration, product=product)
        parameter.save()

        return None

    def _cameo(self, name):
        """Make a name into camel case.

        Arguments:
            name: str

        Returns:
            str
        """

        # split on colons
        camels = []
        sections = name.split('/')
        for section in sections:

            # split by underscores
            words = section.split('_')

            # capitalize each part
            words = [word.capitalize() for word in words]

            # rejoin
            camel = ''.join(words)
            camels.append(camel)

        # connect main name with underscore
        cameo = '_'.join(camels)

        return cameo

    def _categorize(self, chain):
        """Extract categories from h5 file.

        Arguments:
            chain: list of fields

        Returns:
            None
        """

        # parse the chain for attributes
        name = chain.split('/')[-1]
        path = chain
        level = len(chain)

        # print status
        self.stdout.write('creating category {}...'.format(name))

        # get the default configuration
        configuration = self.configuration

        # add the entry to the database
        category, creation = TrendingCategory.objects.get_or_create(name=name, config=configuration, full_path=path)

        # update other attributes
        category.nesting_level = level

        # save category
        category.save()

        # add category to profile
        profile = self.profile
        profile.categories.add(category)
        profile.save()

        return None

    def _establish(self):
        """Establish default profile, product, and configuration.

        Arguments:
            None

        Returns:
            None
        """

        # get base preferences
        user, creation = User.objects.get_or_create(first_name='OMI')
        base, creation = UserProfile.objects.get_or_create(user=user)

        # get profile instance
        profile, creation = TrendingProfile.objects.get_or_create(name='OMI Profile', base_profile=base)

        # get configuration, measurement, and archive
        configuration, creation = TrendingConfig.objects.get_or_create(name='OMI Trending Configuration')
        measurement, creation = MeasurementType.objects.get_or_create(name='OMI')
        archive, creation = ArchiveSet.objects.get_or_create(description='OMI Archive')

        # get matching product instance
        product, creation = TrendingProduct.objects.get_or_create(config=configuration, measurementType=measurement, archive_set=archive)

        # set defaults
        self.product = product
        self.profile = profile
        self.configuration = configuration

        return None

    def _extract(self, group, indent=0):
        """Extract categories and parameters from hdf5 file or group.

        Arguments:
            group: hdf5 file or group
            indent: int, number of fields to skip in full path

        Returns:
            None
        """

        # collect all parameter keys
        keys = list(group.keys())

        # collect all references and addresses
        references = [group[key].ref for key in keys]
        addresses = [group[reference].name for reference in references]

        # collect all categories, removing up to indent
        splittings = [address.split('/')[indent + 1:-1] for address in addresses]
        categories = ['/'.join(splitting[:index + 1]) for splitting in splittings for index in range(len(splitting))]

        # remove duplicates
        categories = list({category: True for category in categories}.keys())

        # add categories
        [self._categorize(category) for category in categories]

        # for each address
        for reference, address in zip(references, addresses):

            # try to
            try:

                # add as a dataset to the Parameters table
                array = group[reference][:]
                chain = address.split('/')[indent + 1:]
                attributes = dict(group[reference].attrs)
                self._parameterize(array, chain, attributes)

            # skip if KeyValue for now
            except (KeyError, AttributeError):

                # print alert
                print('skipping {}!'.format(address))
        #
        # # otherwise assume a group
        # except AttributeError:
        #
        #     # for a nonempty chain
        #     if chain:
        #
        #         # add to the Categories table
        #         self._categorize(chain)
        #
        #     # and continue recursively
        #     fields = list(five.keys())
        #     fields.sort()
        #     for field in fields:
        #
        #         # and continue recursively
        #         self._extract(five[field], chain + [field])

        return None

    def _extract_old(self, five, chain=None):
        """Extract categories and parameters from hdf5 file or group.

        Arguments:
            five: hdf5 file or group=
            chain: list of str, route so far

        Returns:
            None
        """

        # default chain to empty
        chain = chain or []

        # try to
        try:

            # add as a dataset to the Parameters table
            array = five[:]
            attributes = dict(five.attrs)
            self._parameterize(array, chain, attributes)

        # skip if KeyValue for now
        except KeyError:

            # print alert
            print('skipping, KeyError!')

        # otherwise assume a group
        except AttributeError:

            # for a nonempty chain
            if chain:

                # add to the Categories table
                self._categorize(chain)

            # and continue recursively
            fields = list(five.keys())
            fields.sort()
            for field in fields:

                # and continue recursively
                self._extract(five[field], chain + [field])

        return None

    def _parameterize(self, array, chain, attributes):
        """Extract parameters from h5 file.

        Arguments:
            array: hdf5 file dataset
            chain: list of str, previous chain of fields
            attributes: dict, dataset attributes

        Returns:
            None
        """

        # get the name as the last entry, converted to camel case
        #name = self._cameo(chain[-1])
        name = chain[-1]

        # print status
        self.stdout.write('creating parameter {}...'.format(name))

        # get configuration and product
        configuration = self.configuration
        product = self.product

        # create parameter or grab by name
        parameter, creation = TrendingParameter.objects.get_or_create(name=name, config=configuration, product=product)

        # create the data as a list for json formatting
        data = [list(array)]

        # generate a sparkline
        sparkline = self._spark(array)

        # set orbits and dates
        orbits = TrendingParameter.objects.get(name="OrbitNumber")
        dates = TrendingParameter.objects.get(name="OrbitStartTimeFrYr")

        # update data attributes
        parameter.json = data
        parameter.sparkline = sparkline

        # update abscissa references
        parameter.orbits = orbits
        parameter.dates = dates

        # add other attributes
        parameter.FillValue = float(attributes['FillValue'])
        parameter.LongName = attributes['LongName']
        parameter.Units = attributes['Units']
        parameter.Status = attributes['Status']
        parameter.doNotPlot = attributes['doNotPlot']
        parameter.PlotLegend = json.dumps([attributes['PlotLegend']])
        parameter.AbscissaVariableSelection = json.dumps([attributes['AbscissaVariableSelection']])

        # add orbit number
        name = 'OrbitNumber'
        independent, creation = TrendingParameter.objects.get_or_create(name=name, config=configuration, product=product)
        parameter.indvarChoices.add(independent)

        # add year fractions
        name = 'OrbitStartTimeFrYr'
        independent, creation = TrendingParameter.objects.get_or_create(name=name, config=configuration, product=product)
        parameter.indvarChoices.add(independent)

        # save the parameter
        parameter.save()

        # add the category to the parameter table
        category = TrendingCategory.objects.get(full_path='/'.join(chain[:-1]), config=configuration)
        category.parameters.add(parameter)
        category.save()

        # add the parameter to the profile
        profile = self.profile
        profile.parameters.add(parameter)
        profile.save()

        return None

    def _refresh(self, five, path):
        """Refresh the product table with the latest file analyzed.

        Arguments:
            five: hdf5 file
            path: str, filepath

        Returns:
            None
        """

        # get latest orbit number
        orbit = float(five['IndependentVariables']['OrbitNumber'][:][-1])

        # get latest date information
        year = int(five['IndependentVariables']['OrbitStartYr'][:][-1])
        month = int(five['IndependentVariables']['OrbitStartMon'][:][-1])
        day = int(five['IndependentVariables']['OrbitStartDOM'][:][-1])
        hour = int(five['IndependentVariables']['OrbitStartHr'][:][-1])
        minute = int(five['IndependentVariables']['OrbitStartMin'][:][-1])
        second = int(five['IndependentVariables']['OrbitStartSec'][:][-1])

        # construct date
        date = str(datetime(year, month, day, hour, minute, second))

        # get current time, erasing microseconds
        now = str(datetime.now().replace(microsecond=0))

        # get product
        product = self.product

        # update attributes
        product.lastAvailableDate = date
        product.lastRefreshedDate = now
        product.latest_file = path
        product.latest_orbit = orbit

        # save changes
        product.save()

        return None

    def _spark(self, data):
        """Create sparkline from 1-D data.

        Arguments:
            data: hdf5 or numpy array

        Returns:
            sparkline
        """

        # set image attributes in pixels
        width, height = (200, 20)

        # begin image
        image = Image.new("RGB", (width, height), 'white')

        # determine indices for subsampling based on image width
        chunk = len(data) / width
        indices = [floor(index * chunk) for index in range(width)]
        indices = list(set(indices))
        indices.sort()

        # subsample the data,
        sample = data[indices]

        # try to
        try:

            # convert to float and flip the y-axis (PIL images y-axis increases towards bottom)
            sample = [float(datum) * -1 for datum in sample]

        # unless not floats
        except ValueError:

            # set value to 0s
            sample = [0.0] * 20

        # determine extent of y range
        maximum = max(sample)
        minimum = min(sample)
        extent = maximum - minimum

        # calculate a margin and data range
        margin = max([0.1 * extent, 0.001])
        top = maximum + margin
        bottom = minimum - margin

        # in case top and bottom are fills, change gap to 1.0
        gap = (top - bottom) or 1.0

        # translate the data to pixel heights
        pixels = [height * (datum - bottom) / gap for datum in sample]
        points = [(horizontal, vertical) for horizontal, vertical in enumerate(pixels)]

        # make artist and draw the line
        artist = ImageDraw.Draw(image)

        # draw the line in gray
        gray = "#888888"
        artist.line(points, fill=gray)

        # draw a red rectangle at the end
        red = "#FF0000"
        rectangle = [len(sample) - 2, pixels[-1] - 1, len(sample), pixels[-1] + 1]
        artist.rectangle(rectangle, fill=red)

        # create file pointer as bytes
        pointer = BytesIO()
        image.save(pointer, "png")

        # craete sparkline
        sparkline = "data:image/png,{}".format(quote(pointer.getvalue()))

        return sparkline

    def add_arguments(self, parser):

        # parse the arguments
        parser.add_argument('directory')

        return None

    def handle(self, *args, **options):
        """Handle the extract command.

        Arguments:
            *args: unpacked list of args
            **options: unpacked dict

        Returns:
            None
        """

        # establish default configurations, profiles, etc
        self._establish()

        # get directory
        directory = options['directory']

        # get all paths
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        # sort paths
        paths.sort()

        # go through each path
        for path in paths:

            # print
            self.stdout.write('extracting {}...'.format(path))

            # read in  hdf5 file
            five = h5py.File(path, 'r')

            # create dummy abscissa references
            self._calendar()

            # extract independent variables, using it as the category name
            self._extract(five['IndependentVariables'], indent=0)

            # extract categories and parameters from the Data group to preserve order
            self._extract(five['Data'], indent=1)

            # update product model with latest file
            self._refresh(five, path)

            # close file
            five.close()

        return None
