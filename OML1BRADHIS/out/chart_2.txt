Breakdown of ../cmp/OMI-Aura_L1B-OMREDUX_2011m0321t0035-o035529_v0499-2022m0212t1346.h5

( 100.0 % ) IndependentVariables ( 0.1 kb, 14 entries )
          (1,) OrbitEarthSunDistance ( float32 )
          (1,) OrbitEquatorialIndex ( int64 )
          ...( Longitude Number Start Track Hr Min Mon Sec )
          ...( Time Year Yr Length Fr Fraction M Y )
          ...( Yr )

( 100.0 % ) Categories ( 14.34 kb, 2552 entries )
          ( 33.22 % ) BAND1_RADIANCE ( 4.76 kb, 850 entries )
                    ( 33.22 % ) STANDARD_MODE ( 4.76 kb, 850 entries )
                              (1,) OrbitMaxBand1GroundPixel ( float32 )
                              (1,) OrbitMaxBand1NWavelengthPoly ( float32 )
                              ...( Mean Median Min Range Std Nbinningregions Ncorner Nr )
                              ...( Nsettings Scanline Spectral Time Coad Channel Time )

                              ( 3.28 % ) GEODATA ( 0.47 kb, 103 entries )
                                        (1,) OrbitMaxBand1LandWaterClassification ( uint8 )
                                        (1,) OrbitMaxBand1Latitude ( float32 )
                                        ...( Mean Median Min Range Std Longitude Satellite Solar )
                                        ...( Surface Viewing Earth Bounds Altitude Longitude Shadow Azimuth )
                                        ...( Zenith Fraction Sun Phase Fraction Angle Precision Distance )

                              ( 15.58 % ) INSTRUMENT ( 2.23 kb, 451 entries )
                                        (1,) OrbitMaxBand1ProcessingClassProcessingClass ( int16 )
                                        (1,) OrbitMaxBand1Wavelength ( float64 )
                                        ...( Mean Median Min Range Std Coefficient Shift Reference )
                                        ...( Column )

                                        ( 1.47 % ) binning_table ( 0.21 kb, 36 entries )
                                                  (1,) OrbitMaxBand1BinningFactor ( int32 )
                                                  (1,) OrbitMaxBand1DetectorEndRow ( int32 )
                                                  ...( Mean Median Min Range Std Measurement Size Start )

                                        ( 3.02 % ) housekeeping_data ( 0.43 kb, 96 entries )
                                                  (1,) OrbitMaxBand1DetLedStatus ( uint8 )
                                                  (1,) OrbitMaxBand1DifmStatus ( uint8 )
                                                  ...( Mean Median Min Range Std Fmm Sam Temp )
                                                  ...( Wls Det1 Det2 Elu1 Elu2 Elu Opb Opb1 )
                                                  ...( Opb2 Opb3 Opb4 Aux )

                                        ( 0.45 % ) instrument_configuration ( 0.06 kb, 12 entries )
                                                  (1,) OrbitMaxBand1ConfigurationIcId ( int32 )
                                                  (1,) OrbitMaxBand1ConfigurationIcVersion ( int16 )
                                                  ...( Mean Median Min Range Std )

                                        ( 9.05 % ) instrument_settings ( 1.3 kb, 270 entries )
                                                  (1,) OrbitMaxBand1AlternatingMode ( uint8 )
                                                  (1,) OrbitMaxBand1BinnedImageRows ( int16 )
                                                  ...( Mean Median Min Range Std Coaddition Exposure Gain1 )
                                                  ...( Gain2 Gain3 Gain4 Gain Img Instrument Lda Long )
                                                  ...( Lsa Master Measurement Msmt Nr Read Readout Settings )
                                                  ...( Skip Small Stop Uda Usa Period Time Code1 )
                                                  ...( Code2 Code3 Code4 Code Ds Switch Binning Settings )
                                                  ...( Exposure Cycle Class Combination Duration Coadditions Out Ic )
                                                  ...( Rows1 Rows2 Rows3 Rows4 Pixel Column Us Ds )
                                                  ...( Column1 Column2 Column3 Factor Processing Section Period Id )
                                                  ...( Version Column Class Us )

                                        ( 0.49 % ) measurement_to_detector_row_table ( 0.07 kb, 12 entries )
                                                  (1,) OrbitMaxBand1DetEndRow ( int32 )
                                                  (1,) OrbitMaxBand1DetStartRow ( int32 )
                                                  ...( Mean Median Min Range Std )

                              ( 12.81 % ) OBSERVATIONS ( 1.84 kb, 247 entries )
                                        (1,) OrbitMaxBand1DeltaTime ( int32 )
                                        (1,) OrbitMaxBand1QualityLevel ( uint8 )
                                        ...( Mean Median Min Range Std Radiance Observations Error )
                                        ...( Noise Tai93 )

                                        ( 1.96 % ) ground_pixel_quality ( 0.28 kb, 36 entries )
                                                  (1,) OrbitMaxBand1DescendingCount ( float64 )
                                                  (1,) OrbitMaxBand1ErrorCount ( float64 )
                                                  ...( Mean Median Min Range Std Geo Night Solar )
                                                  ...( Sun Bound Eclipse Glint Cross Poss )

                                        ( 4.57 % ) measurement_quality ( 0.66 kb, 84 entries )
                                                  (1,) OrbitMaxBand1AltSeqCount ( float64 )
                                                  (1,) OrbitMaxBand1AltengCount ( float64 )
                                                  ...( Mean Median Min Range Std Coad Irr Msmt )
                                                  ...( Proc Quality Saa Shadow Spacecraft Sub Test Thermal )
                                                  ...( Err Ov Out Comb Skipped Warning Penumbra Manoeuvre )
                                                  ...( Group Instability Of Range )

                                        ( 2.29 % ) spectral_channel_quality ( 0.33 kb, 42 entries )
                                                  (1,) OrbitMaxBand1BadPixelCount ( float64 )
                                                  (1,) OrbitMaxBand1MissingCount ( float64 )
                                                  ...( Mean Median Min Range Std Processing Rts Saturated )
                                                  ...( Transient Underflow Error )

                                        ( 2.61 % ) xtrack_quality ( 0.38 kb, 48 entries )
                                                  (1,) OrbitMaxBand1BlockageEffectCount ( float64 )
                                                  (1,) OrbitMaxBand1CorrectedRowAnomalyCount ( float64 )
                                                  ...( Mean Median Min Range Std Reserved Slight Stray )
                                                  ...( Uncorrected Wavelength For Earthshine Sunlight Shift Future )

          ( 33.22 % ) BAND2_RADIANCE ( 4.76 kb, 850 entries )
                    ( 33.22 % ) STANDARD_MODE ( 4.76 kb, 850 entries )
                              (1,) OrbitMaxBand2GroundPixel ( float32 )
                              (1,) OrbitMaxBand2NWavelengthPoly ( float32 )
                              ...( Mean Median Min Range Std Nbinningregions Ncorner Nr )
                              ...( Nsettings Scanline Spectral Time Coad Channel Time )

                              ( 3.28 % ) GEODATA ( 0.47 kb, 103 entries )
                                        (1,) OrbitMaxBand2LandWaterClassification ( uint8 )
                                        (1,) OrbitMaxBand2Latitude ( float32 )
                                        ...( Mean Median Min Range Std Longitude Satellite Solar )
                                        ...( Surface Viewing Earth Bounds Altitude Longitude Shadow Azimuth )
                                        ...( Zenith Fraction Sun Phase Fraction Angle Precision Distance )

                              ( 15.58 % ) INSTRUMENT ( 2.23 kb, 451 entries )
                                        (1,) OrbitMaxBand2ProcessingClassProcessingClass ( int16 )
                                        (1,) OrbitMaxBand2Wavelength ( float64 )
                                        ...( Mean Median Min Range Std Coefficient Shift Reference )
                                        ...( Column )

                                        ( 1.47 % ) binning_table ( 0.21 kb, 36 entries )
                                                  (1,) OrbitMaxBand2BinningFactor ( int32 )
                                                  (1,) OrbitMaxBand2DetectorEndRow ( int32 )
                                                  ...( Mean Median Min Range Std Measurement Size Start )

                                        ( 3.02 % ) housekeeping_data ( 0.43 kb, 96 entries )
                                                  (1,) OrbitMaxBand2DetLedStatus ( uint8 )
                                                  (1,) OrbitMaxBand2DifmStatus ( uint8 )
                                                  ...( Mean Median Min Range Std Fmm Sam Temp )
                                                  ...( Wls Det1 Det2 Elu1 Elu2 Elu Opb Opb1 )
                                                  ...( Opb2 Opb3 Opb4 Aux )

                                        ( 0.45 % ) instrument_configuration ( 0.06 kb, 12 entries )
                                                  (1,) OrbitMaxBand2ConfigurationIcId ( int32 )
                                                  (1,) OrbitMaxBand2ConfigurationIcVersion ( int16 )
                                                  ...( Mean Median Min Range Std )

                                        ( 9.05 % ) instrument_settings ( 1.3 kb, 270 entries )
                                                  (1,) OrbitMaxBand2AlternatingMode ( uint8 )
                                                  (1,) OrbitMaxBand2BinnedImageRows ( int16 )
                                                  ...( Mean Median Min Range Std Coaddition Exposure Gain1 )
                                                  ...( Gain2 Gain3 Gain4 Gain Img Instrument Lda Long )
                                                  ...( Lsa Master Measurement Msmt Nr Read Readout Settings )
                                                  ...( Skip Small Stop Uda Usa Period Time Code1 )
                                                  ...( Code2 Code3 Code4 Code Ds Switch Binning Settings )
                                                  ...( Exposure Cycle Class Combination Duration Coadditions Out Ic )
                                                  ...( Rows1 Rows2 Rows3 Rows4 Pixel Column Us Ds )
                                                  ...( Column1 Column2 Column3 Factor Processing Section Period Id )
                                                  ...( Version Column Class Us )

                                        ( 0.49 % ) measurement_to_detector_row_table ( 0.07 kb, 12 entries )
                                                  (1,) OrbitMaxBand2DetEndRow ( int32 )
                                                  (1,) OrbitMaxBand2DetStartRow ( int32 )
                                                  ...( Mean Median Min Range Std )

                              ( 12.81 % ) OBSERVATIONS ( 1.84 kb, 247 entries )
                                        (1,) OrbitMaxBand2DeltaTime ( int32 )
                                        (1,) OrbitMaxBand2QualityLevel ( uint8 )
                                        ...( Mean Median Min Range Std Radiance Observations Error )
                                        ...( Noise Tai93 )

                                        ( 1.96 % ) ground_pixel_quality ( 0.28 kb, 36 entries )
                                                  (1,) OrbitMaxBand2DescendingCount ( float64 )
                                                  (1,) OrbitMaxBand2ErrorCount ( float64 )
                                                  ...( Mean Median Min Range Std Geo Night Solar )
                                                  ...( Sun Bound Eclipse Glint Cross Poss )

                                        ( 4.57 % ) measurement_quality ( 0.66 kb, 84 entries )
                                                  (1,) OrbitMaxBand2AltSeqCount ( float64 )
                                                  (1,) OrbitMaxBand2AltengCount ( float64 )
                                                  ...( Mean Median Min Range Std Coad Irr Msmt )
                                                  ...( Proc Quality Saa Shadow Spacecraft Sub Test Thermal )
                                                  ...( Err Ov Out Comb Skipped Warning Penumbra Manoeuvre )
                                                  ...( Group Instability Of Range )

                                        ( 2.29 % ) spectral_channel_quality ( 0.33 kb, 42 entries )
                                                  (1,) OrbitMaxBand2BadPixelCount ( float64 )
                                                  (1,) OrbitMaxBand2MissingCount ( float64 )
                                                  ...( Mean Median Min Range Std Processing Rts Saturated )
                                                  ...( Transient Underflow Error )

                                        ( 2.61 % ) xtrack_quality ( 0.38 kb, 48 entries )
                                                  (1,) OrbitMaxBand2BlockageEffectCount ( float64 )
                                                  (1,) OrbitMaxBand2CorrectedRowAnomalyCount ( float64 )
                                                  ...( Mean Median Min Range Std Reserved Slight Stray )
                                                  ...( Uncorrected Wavelength For Earthshine Sunlight Shift Future )

          ( 33.22 % ) BAND3_RADIANCE ( 4.76 kb, 850 entries )
                    ( 33.22 % ) STANDARD_MODE ( 4.76 kb, 850 entries )
                              (1,) OrbitMaxBand3GroundPixel ( float32 )
                              (1,) OrbitMaxBand3NWavelengthPoly ( float32 )
                              ...( Mean Median Min Range Std Nbinningregions Ncorner Nr )
                              ...( Nsettings Scanline Spectral Time Coad Channel Time )

                              ( 3.28 % ) GEODATA ( 0.47 kb, 103 entries )
                                        (1,) OrbitMaxBand3LandWaterClassification ( uint8 )
                                        (1,) OrbitMaxBand3Latitude ( float32 )
                                        ...( Mean Median Min Range Std Longitude Satellite Solar )
                                        ...( Surface Viewing Earth Bounds Altitude Longitude Shadow Azimuth )
                                        ...( Zenith Fraction Sun Phase Fraction Angle Precision Distance )

                              ( 15.58 % ) INSTRUMENT ( 2.23 kb, 451 entries )
                                        (1,) OrbitMaxBand3ProcessingClassProcessingClass ( int16 )
                                        (1,) OrbitMaxBand3Wavelength ( float64 )
                                        ...( Mean Median Min Range Std Coefficient Shift Reference )
                                        ...( Column )

                                        ( 1.47 % ) binning_table ( 0.21 kb, 36 entries )
                                                  (1,) OrbitMaxBand3BinningFactor ( int32 )
                                                  (1,) OrbitMaxBand3DetectorEndRow ( int32 )
                                                  ...( Mean Median Min Range Std Measurement Size Start )

                                        ( 3.02 % ) housekeeping_data ( 0.43 kb, 96 entries )
                                                  (1,) OrbitMaxBand3DetLedStatus ( uint8 )
                                                  (1,) OrbitMaxBand3DifmStatus ( uint8 )
                                                  ...( Mean Median Min Range Std Fmm Sam Temp )
                                                  ...( Wls Det1 Det2 Elu1 Elu2 Elu Opb Opb1 )
                                                  ...( Opb2 Opb3 Opb4 Aux )

                                        ( 0.45 % ) instrument_configuration ( 0.06 kb, 12 entries )
                                                  (1,) OrbitMaxBand3ConfigurationIcId ( int32 )
                                                  (1,) OrbitMaxBand3ConfigurationIcVersion ( int16 )
                                                  ...( Mean Median Min Range Std )

                                        ( 9.05 % ) instrument_settings ( 1.3 kb, 270 entries )
                                                  (1,) OrbitMaxBand3AlternatingMode ( uint8 )
                                                  (1,) OrbitMaxBand3BinnedImageRows ( int16 )
                                                  ...( Mean Median Min Range Std Coaddition Exposure Gain1 )
                                                  ...( Gain2 Gain3 Gain4 Gain Img Instrument Lda Long )
                                                  ...( Lsa Master Measurement Msmt Nr Read Readout Settings )
                                                  ...( Skip Small Stop Uda Usa Period Time Code1 )
                                                  ...( Code2 Code3 Code4 Code Ds Switch Binning Settings )
                                                  ...( Exposure Cycle Class Combination Duration Coadditions Out Ic )
                                                  ...( Rows1 Rows2 Rows3 Rows4 Pixel Column Us Ds )
                                                  ...( Column1 Column2 Column3 Factor Processing Section Period Id )
                                                  ...( Version Column Class Us )

                                        ( 0.49 % ) measurement_to_detector_row_table ( 0.07 kb, 12 entries )
                                                  (1,) OrbitMaxBand3DetEndRow ( int32 )
                                                  (1,) OrbitMaxBand3DetStartRow ( int32 )
                                                  ...( Mean Median Min Range Std )

                              ( 12.81 % ) OBSERVATIONS ( 1.84 kb, 247 entries )
                                        (1,) OrbitMaxBand3DeltaTime ( int32 )
                                        (1,) OrbitMaxBand3QualityLevel ( uint8 )
                                        ...( Mean Median Min Range Std Radiance Observations Error )
                                        ...( Noise Tai93 )

                                        ( 1.96 % ) ground_pixel_quality ( 0.28 kb, 36 entries )
                                                  (1,) OrbitMaxBand3DescendingCount ( float64 )
                                                  (1,) OrbitMaxBand3ErrorCount ( float64 )
                                                  ...( Mean Median Min Range Std Geo Night Solar )
                                                  ...( Sun Bound Eclipse Glint Cross Poss )

                                        ( 4.57 % ) measurement_quality ( 0.66 kb, 84 entries )
                                                  (1,) OrbitMaxBand3AltSeqCount ( float64 )
                                                  (1,) OrbitMaxBand3AltengCount ( float64 )
                                                  ...( Mean Median Min Range Std Coad Irr Msmt )
                                                  ...( Proc Quality Saa Shadow Spacecraft Sub Test Thermal )
                                                  ...( Err Ov Out Comb Skipped Warning Penumbra Manoeuvre )
                                                  ...( Group Instability Of Range )

                                        ( 2.29 % ) spectral_channel_quality ( 0.33 kb, 42 entries )
                                                  (1,) OrbitMaxBand3BadPixelCount ( float64 )
                                                  (1,) OrbitMaxBand3MissingCount ( float64 )
                                                  ...( Mean Median Min Range Std Processing Rts Saturated )
                                                  ...( Transient Underflow Error )

                                        ( 2.61 % ) xtrack_quality ( 0.38 kb, 48 entries )
                                                  (1,) OrbitMaxBand3BlockageEffectCount ( float64 )
                                                  (1,) OrbitMaxBand3CorrectedRowAnomalyCount ( float64 )
                                                  ...( Mean Median Min Range Std Reserved Slight Stray )
                                                  ...( Uncorrected Wavelength For Earthshine Sunlight Shift Future )

          ( 0.35 % ) PROCESSOR ( 0.05 kb, 2 entries )
                    (1,) CoreProcessorVersion ( |S36 )
                    (1,) ProcessorVersion ( |S16 )
