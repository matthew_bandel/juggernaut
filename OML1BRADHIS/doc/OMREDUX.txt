
ESDT Name: OMREDUX

Processing Level: L1B

Long Name: OMI/Aura Monitoring Data from Reduced OML1BRUG, OML1BRVG Radiance Files,
      1-Orbit

Description: Single orbit one point parameter reductions and quality flag bit decompositions
      of OML1BRUG and OML1BRVG files, reduced to orbital mean, orbital minimum, orbital
      maximum, orbital median, and orbital standard deviation, for the purpose of monitoring
      OMI performance.

Science Team Contacts:

    - Matthew Bandel (matthew.bandel@ssaihq.com)

    - David Haffner (david.haffner@ssaihq.com)

Support Contact: Phillip Durbin (pdurbin@sesda.com)

Minimum Size: 6 MB

Maximum Size: 18 MB

Filename Pattern: <Instrument>-<Platform>_<Processing Level>-<ESDT Name>_<DataDate>-o<OrbitNumber>_v<Collection>-<ProductionTime>.<File
      Format>

Platform: Aura

Instrument: OMI

File Format: h5

Period: Orbits=1

Archive Method: Compress

File SubTable Type: L1L2

Extractor Type: Filename_L1L2

Metadata Type: orbital

Parser: filename

keypattern: <OrbitNumber>
