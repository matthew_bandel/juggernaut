
APP Name: OMREDUX

APP Type: OMI

APP Version: 0.0.12

Long Name: OMI/Aura Monitoring Data from Reduced OML1BRUG, OML1BRVG Radiance Files,
      1-Orbit

Description: 
    
      Single orbit one point parameter reductions and quality flag bit decompositions
      of OML1BRUG and OML1BRVG files, reduced to orbital mean, orbital minimum, orbital
      maximum, orbital median, and orbital standard deviation, for the purpose of monitoring
      OMI performance.
    
      

Lead Algorithm Scientist: Matthew Bandel (matthew.bandel@ssaihq.com)

Other Algorithm Scientists: David Haffner (david.haffner@ssaihq.com)

Software Developer: Matthew Bandel (matthew.bandel@ssaihq.com)

Support Contact: Phillip Durbin (pdurbin@sesda.com)

Structure: 
    
      Run OMREDUX.py as a python program, using the control.txt file path as its argument.
    
      

Operational Scenario: 
    
      Run over entire mission, and for every orbit as new data arrives.
    
      

Period: Orbits=1

EXE:
      Name: OMREDUX.py
      Program: OMREDUX.py

Static Input Files:

    - Filename: flags.txt
      Desc: text file of quality flags and bit designations

Runtime Parameters:

    - Param: AppShortName
      Value: OMREDUX

    - Param: Instrument
      Value: OMI

    - Param: Platform
      Value: Aura

    - Param: OrbitNumber
      Value: <OrbitNumber>

    - Param: StartTime
      Value: <StartTime>

    - Param: ProductionTime
      Value: <ProductionTime>

    - Param: ECSCollection
      Value: <ECSCollection>

    - Param: Maximum Level of Detail
      Value: 2

Production Rules:

    - Rule: GetOrbitParams

    - Rule: OrbitMatch
      ESDT: OML1BRUG
      Min_Files: 1

    - Rule: OrbitMatch
      ESDT: OML1BRVG
      Min_Files: 1

Compilers:

    - CI: python
      Version: 3.6.8

Environment:

    - CI: env_setup
      Version: 0.0.9

    - CI: python
      Version: 3.6.8

Operating System:

    - CI: Linux
      Version: 2.6.9

Input Files:
      OML1BRUG:

      - OMI-Aura_L1-OML1BRUG_2011m0321t0035-o035529_v0499-2021m0313t1048.nc
      OML1BRVG:

      - OMI-Aura_L1-OML1BRVG_2011m0321t0035-o035529_v0499-2021m0313t1048.nc

Output Files:
      OMREDUX:

      - OMI-Aura_L1B-OMREDUX_2011m0321t0035-o035529_v0499-2022m0214t0937.h5
