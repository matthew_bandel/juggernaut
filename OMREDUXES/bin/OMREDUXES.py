#!/usr/bin/env python

# OMREDUXES to stack together single orbit OMREDUX files

# import system
import sys
import subprocess

# if commandline system arguments are found
arguments = sys.argv[1:]
if len(arguments) > 0:

    # pad arguments with blanks as defaults
    arguments += [''] * 5
    arguments = arguments[:5]

    # construct call to juggernaut
    call = ['python3', 'millipedes.py'] + arguments

    # make call with subprocess
    subprocess.call(call)

